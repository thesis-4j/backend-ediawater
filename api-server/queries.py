QUERY_SET_CONSTRAINTS = """ CALL apoc.schema.assert(null,{WaterNode:['id'] , QualityStation:['id']},False) """

QUERY_SET_RELATED_SHP = """ UNWIND $list as data
                            MATCH (:Map)-->(:GISData)-[:COMPOSED_BY]->(f:GISFeature {id: toString(data.sid)}), (p:QualityStation{id:toString(data.pid)})
                            MERGE (p)-[r:RELATED]->(f)
                            RETURN count(r) as update

"""

QUERY_SET_RELATED_SHP_WATERNODE = """ UNWIND $list as data
                            MATCH (:Map)-->(:GISData)-[:COMPOSED_BY]->(f:GISFeature {id: toString(data.sid)}), (p:WaterNode{id:toString(data.pid)})
                            MERGE (p)-[r:RELATED]->(f)
                            RETURN count(r) as update

"""

QUERY_SET_REPRESENTED_SHP = """ UNWIND $list as data
                            MATCH (:Map)-->(:GISData)-[:COMPOSED_BY]->(f:GISFeature {id: toString(data.sid)}), (n:WaterNode{id:toString(data.pid)})
                            MERGE (n)-[r:REPRESENTED]->(f)
                            RETURN count(r) as update

"""


QUERY_SET_CONTAINED_NODE_STATION = """ UNWIND $list as data
                            MATCH (p:QualityStation{id: toString(data.pid)}), (n:WaterNode{id: toString(data.nid)})
                            MERGE (p)-[r:CONTAINED]->(n)
                            RETURN count(r) as update

"""


QUERY_SET_CONTAINED_LANDUSE_WATERSHED = """ UNWIND $list as data
                            MATCH (:GISData{category:"watershed"})-->(w:GISFeature{id: toString(data.wid)}),
                            (:GISData{category:"land-use"})-->(l:GISFeature{id: toString(data.lid)})
                            MERGE (w)-[r:INFO]->(l)
                            RETURN count(r) as update

"""


QUERY_GET_WATERSYSTEM = """ MATCH (n:WaterSystem) RETURN n as watersystem"""

QUERY_GET_WATERSYSTEM_NODES = """ MATCH (n:WaterNode)-[r:NODEOF]->(WaterSystem{name: $name}) RETURN collect(n) AS waternodes """

QUERY_GET_WATERSYSTEM_RELATIONS = """ MATCH ()-[:CONNECTED]-(n:WaterNode)-[:NODEOF]->(WaterSystem{name: $name}) WITH distinct(n) as n1
                                MATCH (n1:WaterNode)-[r:CONNECTED]->(n2:WaterNode)
                                RETURN r.type as type, r.subtype as subtype, r.flow_rate as flow,
                                n1.id as start, apoc.convert.toMap({lat: n1.location.y, lng: n1.location.x}) as startcoord,
                                n2.id as end, apoc.convert.toMap({lat: n2.location.y, lng: n2.location.x}) as endcoord
"""

QUERY_GET_SHP_TYPE = """ MATCH (n:Map)-[:HAS]->(shp) RETURN distinct(shp.category) as types """

QUERY_GET_SHP_GEOJSON_BY_TYPE = """ MATCH (n:Map)-[:HAS]->(:GISData{category:$category})-->(f:GISFeature) RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson """

QUERY_GET_GEOJSON_BY_TYPE_NAME = """ MATCH (n:Map)-[:HAS]->(:GISData{category:$category})-->(f:GISFeature)
                                     WHERE (toLower(f.Name) CONTAINS toLower($name)) OR ( toLower(f.name) CONTAINS toLower($name))
                                     RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""
QUERY_GET_ALL_NODE_SHP = """ MATCH (n:WaterNode)-[:REPRESENTED]->(f:GISFeature)
                            RETURN n.id as nid, apoc.convert.getJsonProperty(f, "geojson") AS geojson, f.id as fid
"""

QUERY_GET_NODE_SHP_BY_ID = """ MATCH (n:WaterNode{id: $id})-[:REPRESENTED]->(f:GISFeature)
                              RETURN n.id as nid, apoc.convert.getJsonProperty(f, "geojson") AS geojson, f.id as fid
"""

QUERY_GET_SHP_OF_WATERSYSTEM = """ MATCH (shp:GISFeature)<-[:REPRESENTED]-(n:WaterNode)-[r:NODEOF]->(WaterSystem{name: $name})
                                   RETURN n as waternode, apoc.convert.getJsonProperty(shp, "geojson") AS geojson
"""


QUERY_GET_ALL_LAYER_LAND = """ MATCH (:GISData{category:"land-use"})-[:COMPOSED_BY]->(f:GISFeature)
                          RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""

QUERY_GET_ALL_LAYER_LAND_ID = """ MATCH (:GISData{category:"land-use"})-->(f:GISFeature)
                          RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson, f.id as id
"""


QUERY_GET_ALL_WATERSHED = """ MATCH (:GISData{category:"watershed"})-[:COMPOSED_BY]->(f:GISFeature)
                              RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""


QUERY_GET_ALL_WATERSHED_ID = """ MATCH (:GISData{category:"watershed"})-->(f:GISFeature)
                              RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson, f.id as id
"""


QUERY_GET_ALL_GEOMETRY = """ MATCH (:GISData{category:"geometry"})-[:COMPOSED_BY]->(f:GISFeature)
                            RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""

QUERY_GET_ALL_GEOMETRY_ID = """ MATCH (:GISData{category:"geometry"})-[:COMPOSED_BY]->(f:GISFeature)
                            RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson, f.id as id
"""


QUERY_GET_FIND_SHAPEFILE = """ MATCH (n:Map)-[:HAS]->(:GISData{category:$category})-[:COMPOSED_BY]->(f:GISFeature{id:$id})
                               RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""

QUERY_GET_FIND_SHAPEFILE_ID = """ MATCH (:GISData)-[:COMPOSED_BY]->(f:GISFeature{id:$id})
                                  RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""

QUERY_GET_WATERNODES = """ MATCH (n:WaterNode) RETURN collect(n) as waternodes """

QUERY_GET_RESERVATORIES = """ MATCH (n:WaterNode{type:'Reservatório'}) RETURN n """

QUERY_GET_DAMS = """ MATCH (n:WaterNode{type:'Barragem'}) RETURN n as waternodes """

QUERY_GET_STATIONS = """ MATCH (n:QualityStation) RETURN  collect(n) as station"""

QUERY_GET_STATIONS_RELATED_ALL_WATERSHED = """ MATCH (s:QualityStation)-[:RELATED]->(w:GISFeature)<--(:GISData {category: "watershed"})
                                               RETURN collect(s) as station
"""

QUERY_GET_STATIONS_RELATED_WATERSHED_ID = """ MATCH (s:QualityStation)-[:RELATED]->(w:GISFeature{id: $id})<--(:GISData {category: "watershed"})
                                              RETURN collect(s) as station
"""


QUERY_GET_STATIONS_RELATED_WATERSHED_NAME = """ MATCH (s:QualityStation)-[:RELATED]->(w:GISFeature)<--(:GISData {category: "watershed"})
                                              WHERE toLower(w.name) CONTAINS toLower($name)
                                              RETURN collect(s) as station
"""


QUERY_GET_PATH_FORWARD = """ MATCH p=(:WaterNode{id: $id})-[:CONNECTED*]->(end:WaterNode)
                             WHERE NOT ((end)-[:CONNECTED]->())
                             RETURN collect(p) as path ,count(p) as finds
"""

QUERY_GET_SOURCE_NODE = """ MATCH path=(source:WaterNode)-[rel:CONNECTED*]->(end:WaterNode{id:$id})
                            WHERE NOT (()-[:CONNECTED]->(source))
                            RETURN path, source
"""

QUERY_GET_END_NODE = """ MATCH path=((:WaterNode{id:$id})-[:CONNECTED*]->(end:WaterNode))
                            WHERE NOT ((end)-[:CONNECTED]->())
                            RETURN path, end
"""

QUERY_GET_FULL_PATH = """ MATCH p=(source:WaterNode)-[:CONNECTED*]->(:WaterNode{id:$id})-[:CONNECTED*]->(end:WaterNode)
                          WHERE NOT (()-[:CONNECTED]->(source)) AND NOT ((end)-[:CONNECTED]->())
                          RETURN collect(p) as path, count(p) as finds
"""

QUERY_GET_FULL_PATH_V1 = """  MATCH p=(source:WaterNode)-[:CONNECTED*]->(:WaterNode{id: $id})-[:CONNECTED*]->(end:WaterNode)
                              WHERE NOT (()-[:CONNECTED]->(source)) AND NOT ((end)-[:CONNECTED]->())
                              WITH apoc.path.elements(p) AS elements
                              UNWIND range(0, size(elements)-2) AS index
                              WITH elements, index
                              WHERE index %2 = 0
                              RETURN elements[index] AS start, elements[index+1] AS relationship, elements[index+2] AS end
"""

QUERY_GET_FULL_PATH_V2 = """ MATCH p=(source:WaterNode)-[:CONNECTED*]->(:WaterNode{id:$id})-[:CONNECTED*]->(end:WaterNode)
                             WHERE NOT (()-[:CONNECTED]->(source)) AND NOT ((end)-[:CONNECTED]->())
                             UNWIND nodes(p) as n


"""


QUERY_GET_FULL_PATH_SHP_V1 = """  MATCH p=(source:WaterNode)-[:CONNECTED*]->(:WaterNode{id:$id})-[:CONNECTED*]->(end:WaterNode)
                                  WHERE NOT (()-[:CONNECTED]->(source)) AND NOT ((end)-[:CONNECTED]->())
                                  WITH apoc.path.elements(p) AS elements
                                  UNWIND range(0, size(elements)-2) AS index
                                  WITH elements, index
                                  WHERE index %2 = 0
                                  MATCH (:WaterNode{id:elements[index].id})-[:REPRESENTED]->(f:GISFeature)
                                  RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""

QUERY_GET_FULL_PATH_SHP_V2 = """  MATCH p=(source:WaterNode)-[:CONNECTED*]->(:WaterNode{id:$id})-[:CONNECTED*]->(end:WaterNode)
                                  WHERE NOT (()-[:CONNECTED]->(source)) AND NOT ((end)-[:CONNECTED]->())
                                  UNWIND nodes(p) as n
                                  MATCH (n)-[:REPRESENTED]->(f:GISFeature)
                                  RETURN apoc.convert.getJsonProperty(f, "geojson") AS geojson
"""


QUERY_GET_NEARBY_WATERNODE = """
                                  OPTIONAL MATCH (p2:WaterNode{id:$id})-[r2:CONNECTED]->(n2:WaterNode)
                                  OPTIONAL MATCH (n1:WaterNode)-[r1:CONNECTED]->(p:WaterNode{id:$id})
                                  RETURN apoc.convert.toList([{from: p2, info: r2, to:n2 },{from:n1, info:r1, to:p}]) as nearby
"""

QUERY_GET_NODE_LABEL = """ MATCH (n) WHERE size(labels(n)) >0
                           WITH DISTINCT labels(n) AS labels
                           UNWIND labels AS label
                           RETURN DISTINCT label
                           ORDER BY label
"""

QUERY_GET_NODE_ATT = """ MATCH (p:%s)
                    WITH p, keys(p) as att
                    limit 1
                    UNWIND att as key
                    RETURN distinct key, apoc.map.get(apoc.meta.cypher.types(p), key, [true]) as type
"""

QUERY_GET_REL_ATT = """ MATCH ()-[p:%s]-()
                        WITH distinct p, keys(p) as pKeys
                        UNWIND pKeys as Key
                        RETURN distinct Key as key, apoc.map.get(apoc.meta.cypher.types(p), Key, [true]) as type

"""

QUERY_GET_ALL_WATER_REL = """ MATCH (n1:WaterNode)-[r:CONNECTED]->(n2:WaterNode)
                              RETURN r.type as type, r.subtype as subtype, r.flow_rate as flow,
                              n1.id as start, apoc.convert.toMap({lat: n1.location.y, lng: n1.location.x}) as startcoord,
                              n2.id as end, apoc.convert.toMap({lat: n2.location.y, lng: n2.location.x}) as endcoord
"""

QUERY_GET_DETACH = """ MATCH (n) WHERE NOT (n)--() RETURN n """


QUERY_FIND_ALL_STRING = """  MATCH (n:WaterNode)
                      WHERE (any(prop in keys(n) where  n[prop]  CONTAINS $string))
                      RETURN collect(n) as result, COUNT(n) as finds;
"""


QUERY_FIND_NODE_ATT = """ MATCH (n:%s) WHERE %s RETURN n """

QUERY_FIND_REL_ATT = """ MATCH ()-[r:%s]->() WHERE %s RETURN n1,n2,r """

QUERY_LOAD_STATION = """ WITH $json as list
                         UNWIND list as obj
                         MERGE (n:QualityStation{id:obj.id})
                         SET n += obj
                         RETURN count(obj) as upload
"""


QUERY_GET_STATION_DATA = """ MATCH (data)-[:COLLECTED]->(n:QualityStation{id:$id}) RETURN data """

QUERY_GET_STATION_DATA_MULTIPLE_ID = """
  WITH $json as list
  UNWIND list as id
  MATCH (data:QualityData)-[:COLLECTED]->(n:QualityStation{id: id})
  RETURN  id, data
"""


QUERY_GET_WATERDATA_ATT_ID = """
  MATCH (q:QualityStation {id: $id})<-[:COLLECTED]-(p:QualityData) WITH p, keys(p) as att limit 1
  UNWIND att as key
  RETURN distinct key, apoc.map.get(apoc.meta.cypher.types(p), key, [true]) as type
"""


QUERY_GET_MULTIPLE_WATERDATA_ATT = """
  WITH $json as list
  UNWIND list as sid
  MATCH (q:QualityStation {id: sid})<-[:COLLECTED]-(p:QualityData)
  WITH p, keys(p) as att
  UNWIND att as key
  return distinct key as name, count(key) as count ORDER BY count DESC
"""

QUERY_GET_ATT_DATA_WATERDATA = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as key
  WITH  key, q, p
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL
  RETURN distinct q.id  as id, collect(apoc.convert.toMap({name: key, value: p[key], date: p['date'] })) as data
"""

QUERY_GET_DATA_OBJ_PERSTATION = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj, obj.date as date
  UNWIND obj.attributes as key
  WITH  key, q, p
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL AND p['date'] = date
  WITH distinct q.id as id, key, avg(toFloat(p[key])) as values
  WITH id, collect(distinct key) as keys, collect(apoc.map.fromValues([key,values,"id", id ])) as listdata
  return  keys, collect(apoc.map.mergeList(listdata)) as data
"""


QUERY_GETT_DATA_OBJ_PERSTATION_AVG = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as key
  WITH  key, q, p
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL
  WITH distinct q.id as id, key, avg(toFloat(p[key])) as values
  WITH id, collect(distinct key) as keys, collect(apoc.map.fromValues([key,values,"id", id ])) as listdata
  return  keys, collect(apoc.map.mergeList(listdata)) as data
"""

QUERY_GETT_DATA_OBJ_PERSTATION_MIN = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as key
  WITH  key, q, p
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL
  WITH distinct q.id as id, key, min(toFloat(p[key])) as values
  WITH id, collect(distinct key) as keys, collect(apoc.map.fromValues([key,values,"id", id ])) as listdata
  return keys, collect(apoc.map.mergeList(listdata)) as data
"""

QUERY_GETT_DATA_OBJ_PERSTATION_MAX = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as key
  WITH  key, q, p
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL
  WITH distinct q.id as id, key, max(toFloat(p[key])) as values
  WITH id, collect(distinct key) as keys, collect(apoc.map.fromValues([key,values,"id", id ])) as listdata
  return keys, collect(apoc.map.mergeList(listdata)) as data
"""


QUERY_GET_ATT_DATA_PERSTATION_MIN = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as keys
  WITH  keys, q, p
  WHERE toString(p[keys]) <> 'NaN' AND p[keys] IS NOT NULL
  WITH distinct q.id  as id, keys as name,  min(p[keys]) as value
  return id, collect(apoc.convert.toMap({name: name, value: value })) as data
"""


QUERY_GET_ATT_DATA_PERSTATION_MAX = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as keys
  WITH  keys, q, p
  WHERE toString(p[keys]) <> 'NaN' AND p[keys] IS NOT NULL
  WITH distinct q.id  as id, keys as name,  max(p[keys]) as value
  return id, collect(apoc.convert.toMap({name: name, value: value })) as data
"""


QUERY_GET_ATT_DATA_PERSTATION_AVG = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as keys
  WITH  keys, q, p
  WHERE toString(p[keys]) <> 'NaN' AND toString(p[keys]) <> ' ' AND p[keys] IS NOT NULL
  WITH distinct q.id  as id, keys as name, AVG(toFloat(p[keys])) as value
  return id, collect(apoc.convert.toMap({name: name, value: value })) as data
"""

QUERY_GET_ATT_DATA_PERATT_MIN = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH p, obj
  UNWIND obj.attributes as keys
  WITH  keys, p
  WHERE toString(p[keys]) <> 'NaN' AND toString(p[keys]) <> ' ' AND p[keys] IS NOT NULL
  WITH  keys as name,  min(toFloat(p[keys])) as value
  return name, value
"""


QUERY_GET_ATT_DATA_PERATT_MAX = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH p, obj
  UNWIND obj.attributes as keys
  WITH  keys, p
  WHERE toString(p[keys]) <> 'NaN' AND toString(p[keys]) <> ' ' AND p[keys] IS NOT NULL
  WITH  keys as name,  max(toFloat(p[keys])) as value
  return name, value
"""

QUERY_GET_ATT_DATA_PERATT_AVG = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH p, obj
  UNWIND obj.attributes as keys
  WITH  keys, p
  WHERE toString(p[keys]) <> 'NaN' AND toString(p[keys]) <> ' ' AND p[keys] IS NOT NULL
  WITH  keys as name,  avg(toFloat(p[keys])) as value
  return name, value
"""


QUERY_GET_TIMESERIES_XY_ATT_DATA = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as key
  WITH  key, q, p ORDER BY apoc.date.parse(p.date, "ms", "dd/MM/yyy")
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL
  WITH  distinct key, q, collect(apoc.map.fromValues(["x",p.date,"y",p[key]])) as data
  RETURN key+'-'+q.id as id, q.id as key, data, ['date', 'value'] as labels
"""

QUERY_GET_TIMESERIES_XY_ATT_DATA_RANGE = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj, obj.range as range
  UNWIND obj.attributes as key
  WITH  key, q, range, p ORDER BY apoc.date.parse(p.date, "ms", "dd/MM/yyy")
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL AND  apoc.date.parse(p.date, "ms", "dd/MM/yyyy") >= apoc.date.parse(range.min, "ms", "dd/MM/yyyy") and  apoc.date.parse(p.date, "ms", "dd/MM/yyyy") <= apoc.date.parse(range.max, "ms", "dd/MM/yyyy")
  WITH  key, q, collect(apoc.map.fromValues(["x",p.date,"y",p[key]])) as data
  RETURN distinct key as id, q.id as key, data, ['date', 'value'] as labels
"""

QUERY_GET_BUBBLE_CHILDREN_ATT_DATA = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as key
  WITH  key, q, p
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL
  WITH distinct p[key] as idx, count(p[key])  as valuex, q.id as id, key
  WITH  key as idy, id, collect(apoc.map.fromValues(["id",idx,"value",valuex])) as childx
  WITH collect(apoc.map.fromValues(["id",idy,"value",size(childx),"children",childx]) ) as children, id
  RETURN id, children, size(children) as value
"""

QUERY_GET_ATT_DATA_DATES = """
  WITH $json as list
  UNWIND list as obj
  MATCH (q:QualityStation {id: obj.id})<-[:COLLECTED]-(p:QualityData)
  WITH q, p, obj
  UNWIND obj.attributes as key
  WITH  key, q, p
  WHERE toString(p[key]) <> 'NaN' AND p[key] IS NOT NULL
  WITH distinct p.date as date
  WITH date ORDER BY  apoc.date.parse(date, "ms", "dd/MM/yyy")
  RETURN collect(date) as dates
"""


QUERY_LOAD_WATER = """
                       WITH $json as list
                       UNWIND list as csv
                       MERGE (s:WaterSystem{name: csv.watersystem})
                       MERGE (node:WaterNode{id: csv.id})
                       SET node.location = Point({latitude: toFloat(csv.latitude) , longitude: toFloat(csv.longitude) , height: toFloat(csv.height), crs: 'wgs-84-3d'}), node.name=csv.name, node.type=csv.type, node.subtype=csv.subtype,
                       node.tag=csv.tag, node.power=csv.power, node.fwl=csv.fwl
                       MERGE (node)-[r:NODEOF]->(s)
                       RETURN count(r) as upload
"""

QUERY_LOAD_HYDRO = """
                         WITH $json as list
                         UNWIND list as obj
                         MERGE (node:HydroNode{id: obj.id})
                         SET node += obj
                         MERGE (node)-[r:NODEOF]->(s:WaterSystem{name: obj.watersystem})
                         RETURN count(node) as upload
"""

QUERY_LOAD_ALLTYPE_NODES = """
                              WITH $json as list
                              UNWIND list as csv
                              CALL apoc.do.case([toLower(csv.type) = 'aproveitamento hidrográfico', 'MERGE (node:HydroNode{id: csv.id}) set node += csv RETURN node' ],
                              'MERGE (node:WaterNode{id: csv.id})
                              SET node.location = Point({latitude: toFloat(csv.latitude) , longitude: toFloat(csv.longitude) , height: toFloat(csv.height), crs: "wgs-84-3d"}), node.name=csv.name, node.type=csv.type, node.subtype=csv.subtype,
                              node.tag=csv.tag, node.power=csv.power, node.fwl=csv.fwl RETURN node',{csv:csv}) yield value
                              WITH value.node as node,csv
                              MERGE (s:WaterSystem{name: csv.watersystem})
                              MERGE (node)-[r:NODEOF]->(s)
                              RETURN count(r) as upload
"""


QUERY_LOAD_RELATIONS = """ WITH $json as list
                           UNWIND list as obj
                           MATCH (n1:WaterNode{id:obj.start}), (n2:WaterNode{id:obj.end})
                           MERGE (n1)-[r:CONNECTED{}]->(n2)
                           SET r += {type: obj.type, subtype: obj.subtype, flow_rate: obj.flow_rate}
                           RETURN count(r) as upload
"""
#  missing associated and connect between stations

QUERY_LOAD_RELATIONS_ALL = """ WITH $json as list
                               UNWIND list as row
                               CALL apoc.do.case([row.relation = "CONTAINED",'MATCH (s:QualityStation{id:row.start}), (w:WaterNode{id:row.end}) MERGE (s)-[r:CONTAINED]->(w) RETURN r',
                                                  row.relation = "RELATED",'MATCH (f:GISData{category:"watershed"})-->(f:GISFeature{id:row.end}), (n) WHERE (n:WaterNode OR n:QualityStation) AND n.id=row.start MERGE (n)-[r:RELATED]->(f) RETURN r',
                                                  row.relation = "REPRESENTED",'MATCH (w:WaterNode{id:row.start}), (f:GISData{category:"geometry"})-->(f:GISFeature{id:row.end}) MERGE (w)-[r:REPRESENTED]->(f) RETURN r',
                                                  row.relation = "CONNECTED",'MATCH (n1:WaterNode{id:row.start}),(n2) WHERE (n2:WaterNode OR n2:HydroNode) AND n.id=row.end
                                                                  MERGE (n1)-[r:CONNECTED {type: row.type, subtype: row.subtype, flow_rate: toFloat(row.flow_rate)}]->(n2) RETURN r'],
                                                  row.relation = "MONITOR_WATERCOURSE",'MATCH (n1:WaterNode{id:row.start}),(n2:WaterNode{id:row.start}), (s:QualityStation{id:row.id})
                                                                  MERGE (n1)<-[r:MONITOR_WATERCOURSE]-(s)-[r2:MONITOR_WATERCOURSE]->(n2) RETURN r']
                                                ,'',{row:row}) YIELD value
                               RETURN count(value.r) as upload
"""


QUERY_LOAD_DATA = """ WITH $json as list
                      UNWIND list as obj
                      MATCH (n:QualityStation) WHERE (obj.snirh CONTAINS n.snirh AND obj.snirh is not null) OR (n.id = obj.id)
                      WITH n, obj
                      CREATE (data:QualityData)
                      SET data += obj
                      CREATE (data)-[r:COLLECTED]->(n)
                      RETURN count(r) as relations, count(data) as data
"""

QUERY_LOAD_DATA_V2 = """ call apoc.periodic.iterate(
  "WITH $json as list UNWIND list as obj
   MATCH (n:QualityStation) WHERE (obj.snirh CONTAINS n.snirh AND obj.snirh is not null) OR (n.id = obj.id)
   RETURN n, obj",
  "CREATE (data:QualityData) SET data += obj
   CREATE (data)-[r:COLLECTED]->(n) RETURN count(r) as relations, count(data) as data",
  {batchSize: 1000,iterateList: true, params: {json:$json}})
"""


QUERY_LOAD_DATA_V3 = """ call apoc.periodic.commit(
  'WITH $json as list UNWIND list as obj
  MATCH (n:QualityStation) WHERE (obj.snirh CONTAINS n.snirh AND obj.snirh is not null) OR (n.id = obj.id)
  WITH n, obj limit $limit
  CREATE (data:QualityData) SET data += obj
  CREATE (data)-[r:COLLECTED]->(n)
  RETURN count(*)', {json: $json, limit: 10000}) YIELD updates
  RETURN *
"""

QUERY_LOAD_SHP_GEOJSON = """ WITH $geojson as json
                             MERGE (shp:GISData{filename: $filename, type: json.type, category:$category})
                            ON CREATE SET shp.created = datetime(), shp.description=$description
                            MERGE (map:Map)
                            SET map.lastid = id(shp), map.lastupdate = shp.created, map.lastfile=shp.filename
                            MERGE (map)-[:HAS]->(shp)
                            WITH shp, json
                            UNWIND json.features as features
                            MERGE (f:GISFeature {Name: features.properties.Name, type: features.type})
                            SET f += features.properties
                            WITH shp, f, features
                            SET f.id = CASE WHEN features.properties.id is null THEN apoc.create.uuid() END
                            WITH shp, f, features
                            CALL apoc.convert.setJsonProperty(f, 'geojson',features)
                            MERGE (shp)-[:COMPOSED_BY]->(f)
                            RETURN f.name as name , f.id as id, features.type as type, shp.filename as filename
"""

QUERY_LOAD_SHP_GEOJSON_V1 = """ WITH $geojson as json
                            MERGE (shp:GISData{type: json.type, category:$category})
                            ON CREATE SET shp.created = datetime(), shp.description=$description, shp.filename= $filename
                            ON MATCH SET shp.created = datetime(), shp.description=$description, shp.filename= $filename
                            MERGE (map:Map)
                            SET map.lastid = id(shp), map.lastupdate = shp.created, map.lastfile=shp.filename
                            MERGE (map)-[:HAS]->(shp)
                            WITH shp, json
                            UNWIND json.features as features
                            MERGE (shp)-[:COMPOSED_BY]->(f:GISFeature {name: features.properties.name, type: features.type})
                            SET f += features.properties
                            WITH shp, f, features
                            SET f.id = CASE WHEN features.properties.id is null THEN apoc.create.uuid() END
                            WITH shp, f, features
                            CALL apoc.convert.setJsonProperty(f, 'geojson',features)
                            RETURN f.name as name , f.id as id, features.type as type, shp.filename as filename
"""


QUERY_LOAD_SHP_GEOJSON_V2 = """ WITH $geojson as json
                            MERGE (shp:GISData{type: json.type, category:$category})
                            SET shp.updated = datetime(),  shp.lastfile = $filename
                            MERGE (map:Map)
                            SET map.lastid = id(shp), map.lastupdate = shp.created, map.lastfile=shp.filename
                            MERGE (file:File{name:$filename})
                            SET file.description=$description
                            MERGE (file)-[:UPLOADED_TO]->(map)-[:HAS]->(shp)
                            WITH shp, json, file
                            UNWIND json.features as features
                            MERGE (shp)-[:COMPOSED_BY]->(f:GISFeature {name: features.properties.name, type: features.type})<-[:FROM]-(file)
                            SET f += features.properties
                            WITH shp, f, features
                            SET f.id = CASE WHEN features.properties.id is null THEN apoc.create.uuid() END
                            WITH shp, f, features
                            CALL apoc.convert.setJsonProperty(f, 'geojson',features)
                            RETURN f.name as name , f.id as id, features.type as type, shp.filename as filename
"""

QUERY_LOAD_DATA_RAW = """ LOAD CSV WITH HEADERS FROM $URI as obj
                          MATCH (n:QualityStation) WHERE (obj.SNIRH CONTAINS n.SNIRH) OR (n.EDIA=obj.EDIA)
                          WITH n, obj
                          CREATE (data:QualityData)
                          SET data += obj
                          MERGE (data)-[r:COLLECTED]->(n)
                          RETURN count(r) as relations, count(data) as data
"""


QUERY_DELETE_WATER_RELATION = """ MATCH (n:WaterNode{id: $id1})-[r:CONNECTED]-(n2:WaterNode{id: $id2}) DELETE r """

QUERY_DELETE_WATERNODE = """ MATCH (n3:WaterNode)<-[r3:CONNECTED]-(n:WaterNode{id: $id})<-[r2:CONNECTED]-(n2:WaterNode)
                            WITH n3, r2, n2, n
                            CREATE (n3)<-[r:CONNECTED]-(n2)
                            SET r += r2
                            DETACH DELETE n
"""

QUERY_ADD_NEW_WATERNODE_RELATION = """ MATCH (n1:WaterNode{id: $startid}),(n2:WaterNode{id: $endid})
                                       WHERE NOT ((n1)-[r:CONNECTED]-(n2))
                                       CREATE (n1)-[r:CONNECTED]->(n2)
                                       RETURN count(r) as created
"""

QUERY_ADD_NEW_MIDDLE_NODE_RELATION = """  MATCH (n1:WaterNode{id:$endid})<-[r:CONNECTED]-(n2:WaterNode{id:$startid})
                                          WITH r,n1, n2, $json as json
                                          CREATE (t:WaterNode{id: $newid})
                                          SET t += json
                                          CREATE (n1)<-[r1:CONNECTED]-(t)<-[r2:CONNECTED]-(n2)
                                          SET r1 += r, r2 += r
                                          DELETE r
                                          RETURN n as end,r1,t as new,r2,n2 as start

"""

########## EXAMPLE #############

""" This test query is for json file with a json object per line
    ex:
            {obj1}
            {obj2}
            {obj3}

"""
QUERY_STATION_TEST = """CALL apoc.load.json("file:///stations.json")
                        YIELD value
                        MERGE (n:QualityStation{SNIRH:value.SNIRH})
                        SET n += value
                        RETURN count(n)
"""
