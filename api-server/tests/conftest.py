import sys
sys.path.append('..')
import app as server
import pytest

@pytest.fixture
def app():
    yield server.app

@pytest.fixture
def client(app):
    return app.test_client()

