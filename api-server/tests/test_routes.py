import json
import sys
import pytest

sys.path.append('..')
from app import Neo4jx

"""
    Unit tests
    conftest.py setup:
        app = Flask(__name__)
        configure_routes(app)
        client = app.test_client()
    All test funtions need to start by the prefix: test_

"""

DRIVER_SESSION = (Neo4jx.session is None)

def test_base_route(app,client):
    url = '/'

    response = client.get(url)

    assert response.status_code == 200


def test_database_driver_route(app,client,capsys):
	url = '/driver/database'

	response = client.get(url)

	assert response.status_code == 200


@pytest.mark.skipif(DRIVER_SESSION,
                    reason='Database driver session not established')
def test_public_waternode_route(app,client):
	url = '/api/waternode'

	response = client.get(url)
	data = response.get_json()

	assert (type(data[0]['waternodes']) is list) == True
	assert response.status_code == 200


@pytest.mark.skipif(DRIVER_SESSION,
                    reason='Database driver session not established')
def test_public_waternode_relation_route(app,client):
	url = '/api/waternode/relation'

	response = client.get(url)
	data = response.get_json()

	assert (type(data) is list) == True
	assert response.status_code == 200


@pytest.mark.skipif(DRIVER_SESSION,
                    reason='Database driver session not established')
def test_public_waterquality_station_route(app,client):
	url = '/api/water-quality/station/'

	response = client.get(url)
	data = response.get_json()

	assert (type(data[0]['station']) is list) == True
	assert response.status_code == 200
