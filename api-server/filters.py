import os
import datetime
import pandas as pd


def read_topandas(file, json=True, lower=False, nanval=False):
    try:
        df = pd.read_csv(file, dtype='object')
        if lower:
            df.rename(columns=str.lower, inplace=True)
        if nanval:
            df.fillna(0, inplace=True)
        if json:
            return df.to_dict(orient='records'), df
        return df

    except IOError as e:
        print(e)
    return {}, None


def pandas_ruler_FloatRule(row):
    try:
        float(row)
        return True
    except ValueError:
        return False


def pandas_ruler_haveNull(df):
    return df.isnull().values.any()


def pandas_ruler_isNumCol(df):
    test = pd.DataFrame()
    test['float'] = df.apply(pandas_ruler_FloatRule)

    for i in range(df.shape[0]):
        if test['float'][i] == False:
            return False

    return True


def pandas_ruler_DateRule(row):
    try:
        d, m, y = row.split('/')
        datetime.datetime(int(y), int(m), int(d))
        return True
    except ValueError:
        return False


def pandas_ruler_validDate(df):
    test = pd.DataFrame()
    test['bool'] = df.apply(pandas_ruler_DateRule)

    for i in range(df.shape[0]):
        if test['bool'][i] == False:
            return False
    return True


def pandas_notsatisfied(df_orig, df_finale):
    return list(df_orig.index.difference(df_finale.index))
