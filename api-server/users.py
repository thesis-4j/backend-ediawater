import bcrypt
import uuid
from typing import List


class User(object):
    def __init__(self, username: str, password: str, defaults={}):
        self.id = str(uuid.uuid4())
        self.username = username
        self.password = self.hash_pwd(password)
        self.defaults = defaults

    def __str__(self):
        return "User(id='%s')" % self.id

    def hash_pwd(self, password: str):
        salt = bcrypt.gensalt()
        return bcrypt.hashpw(password.encode('utf-8'), salt)

    def verify(self, input_password: str):
        return bcrypt.checkpw(input_password.encode('utf-8'), self.password)


class SafeAuth(object):
    def __init__(self, users: List[User] = []):
        self.users = users
        self.username_table = {u.username: u for u in users}
        self.id_table = {u.id: u for u in users}

    def authenticate(self, username, password):
        user = self.username_table.get(username, None)
        if user and user.verify(password):
            return True
        return False

    def identity(self, user_id):
        return self.id_table.get(user_id, None)

    def add(self, user: User):

        try:
            if self.username_table.get(user.username, None):
                return False

            self.users.append(user)
            index = len(self.users)-1

            self.username_table[user.username] = self.users[index]
            self.id_table[user.id] = self.users[index]

        except (KeyError, TypeError):
            raise TypeError("The user class is not valid. Should have username, password and id fields")

        return True
