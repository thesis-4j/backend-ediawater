from flask import jsonify
from neo4j import GraphDatabase as GDB, basic_auth, exceptions


class Neo4jConn():
    """ A Neo4j driver wrapper.

        This class cames to simplify the following example
        of normal usage of this neo4j python driver:

        ses = None

        try:
            print("Connecting driver ...")
            # driver's database connection
            drv = GDB.driver(ADDR, auth=basic_auth(
                USR, PWD), connection_timeout=TIMEOUT)
            ses = drv.session()  # session opening
            print("Connection established.")
            try:
                ses.run(queries.QUERY_SET_CONSTRAINTS)
            except exceptions.ClientError:
                print("WARNING: Update Neo4j configuration file needed.")
        except exceptions.ServiceUnavailable as exc:
            ses = None
            print("WARNING:", exc)
    """

    def __init__(self, ip, port, addr, usr, pwd, timeout=100.0):
        self.ip = ip
        self.port = port
        self.addr = addr
        self.usr = usr
        self.pwd = pwd
        self.timeout = timeout
        self.driver = None
        self.session = None

    def start(self, start_query=""):
        print("Neo4jConn() Connecting driver ...")
        self.connect()  # driver's database connection
        self.open()  # open session

        if self.session is not None:
            print("Neo4jConn() Connection established.")

            if start_query:
                try:
                    self.session.run(start_query)
                except exceptions.ClientError:
                    print("Neo4jConn() WARNING: Update Neo4j configuration file needed.")

    def connect(self):
        try:
            self.driver = GDB.driver(self.addr, auth=basic_auth(
                self.usr, self.pwd), connection_timeout=self.timeout)
        except exceptions.ServiceUnavailable as exc:
            self.session = None
            print("Neo4jConn() WARNING:", exc)

    def open(self):
        if self.driver is None:
            self.connect()
        try:
            self.session = self.driver.session()
        except (AttributeError, exceptions.ServiceUnavailable):
            print("Neo4jConn() WARNING: Session was not created.")

    def restart(self):
        self.connect()  # driver's database connection
        self.open()  # open session

    def execute(self, query, jsonObj):
        if self.session is None:
            self.open()
        try:
            try:
                res = self.session.run(query, json=jsonObj)
            except ValueError:
                return None, 102  # 102 Still processing

            if res is None:
                self.restart()
                res = self.session.run(query, json=jsonObj)

            return res.data(), 200

        except (AttributeError, exceptions.ClientError) as err:
            print(err)
            return jsonify("This problem can be related to database connection or query error. Please, contact the service's Admin"), 504
