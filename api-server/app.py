# -*- coding: utf-8 -*-
"""

Todo:
        * You have to also use ``sphinx.ext.todo`` extension
        * Add auth and encryptation of raw data loads
        * Loads and Update endpoints should be private and secure
        * Endpoint to return public endpoints

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html
.. _Napoleon:
   https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html

"""


import os
import secrets
import simplejson as json
import requests
import pandas as pd
import geopandas as gpd
import geojson_utils as geojson

from flask import Flask, flash, abort, request, jsonify
from flask_cors import CORS, cross_origin
from flask_caching import Cache
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager
from flask_executor import Executor

from fiona.io import ZipMemoryFile

import queries
from filters import read_topandas, pandas_ruler_haveNull, pandas_ruler_isNumCol, pandas_notsatisfied
from verify import validCSV
from users import User, SafeAuth

from neo4jwr import Neo4jConn

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 32 * 1024 * 1024
CORS(app, resources={r"/*": {"origins": "*"}})

app.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = False
executor = Executor(app)

app.secret_key = os.getenv('SECRET_KEY', str(b'_5#y2L"F4Q8z\n\xec]/'))
app.config['SESSION_TYPE'] = 'filesystem'

app.config["JWT_SECRET_KEY"] = str(secrets.token_bytes())
jwt = JWTManager(app)  # Setup the Flask-JWT-Extended extension

app.config['CACHE_TYPE'] = 'simple'  # 'SimpleCache'
app.cache = Cache(app)

base = SafeAuth()
test_user = User("test", "test")
base.add(test_user)

#################################################
# Database session
#################################################

IP = os.getenv('IP_NEO4J', "localhost")
PORT = os.getenv('PORT_NEO4J', "7687")
ADDR = "bolt://" + IP + ":" + PORT
USR = os.getenv('USR_NEO4J', "neo4j")
PWD = os.getenv('PWD_NEO4J', "malbusca1995")
TIMEOUT = 50.0  # max heroku's worker timout connection


Neo4jx = Neo4jConn(ip=IP, port=PORT, addr=ADDR, usr=USR, pwd=PWD, timeout=TIMEOUT)
Neo4jx.start(queries.QUERY_SET_CONSTRAINTS)

#################################################
# Constants
#################################################

ALLOWED_EXTENSIONS = set(['shp', 'csv', 'json', 'geojson', 'zip'])
BLACKLIST = ["detach", "delete", "merge"]
SERVER_VERSION = os.getenv('SERVER_VERSION', 'vX.X.X')
CORS_HEADER = {'Content-Type': 'application/json', 'headers': {"Access-Control-Allow-Origin": "*"}}


#################################################
# Functions
#################################################

def prepare_cypher(string: str):
    try:
        for s in string.split(" "):
            # print(s)
            if s.lower() in BLACKLIST:
                return True
    except TypeError:
        # print(e)
        pass

    return False


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def load_post_multifiles(req):
    try:
        data_header = json.loads(req.form.get('JSON'))
        files = []
        for file in data_header['data']:
            files.append(
                {file['name']: read_topandas(req.files[file['name']])})
        return files

    except requests.exceptions.RequestException as e:
        print(e)
    return []


def return_response(resp, info=None, debug=False):

    if 'error' in resp:
        return abort(404)

    try:
        if info:
            if isinstance(resp, list):
                resp[0]['info'] = info

            elif isinstance(resp, dict):
                resp['info'] = info

            else:
                newData = {'response': resp, 'info': info}
                resp = newData

    except (KeyError, TypeError):
        pass

    if debug:
        print(resp)

    return json.dumps(resp, ensure_ascii=False, encoding='latin1', ignore_nan=True), 200, CORS_HEADER


def return_error(code, message=""):
    print(message)
    flash(message)
    return message, 406


def convert_toPoint(longitude, latitude):
    try:
        return {"type": "Point", "coordinates": [float(longitude), float(latitude)]}
    except ValueError:
        print("Converting error at coordinates: [float(%f), float(%f)]" % (
            longitude, latitude))

    return None


#################################################
# Exception handling section
#################################################
# Define error messages for which exception


@app.errorhandler(404)
def page_not_found(exception):
    return "Sorry, that a resource is not found.", 404


@app.errorhandler(401)
def authentication_failed(exception):
    return "Sorry, the user isn’t not authorized to access a resource", 401


@app.errorhandler(403)
def unauthorized_access(exception):
    return "Sorry,  the user is authenticated, but it’s not allowed to access a resource.", 403


@app.errorhandler(500)
def internal_error(exception):
    return str(exception), 500


#################################################
# API endpoint section
#################################################


@app.route('/', methods=['GET', 'POST'])
def server_rv():
    """ API endpoint that returns sucessful response if accessed by a `GET` and `POST`
            Args:
                no value
            Returns:
                Returns a string and a successful code 20x if the server is available
    """
    return 'Ediwater API server ' + SERVER_VERSION, 200


@app.route("/login", methods=["POST"])
def login():
    username = request.json.get("username", None)
    password = request.json.get("password", None)
    if not base.authenticate(username, password):
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token)


@app.route("/protected", methods=["GET"])
@jwt_required()
def protected():
    # Access the identity of the current user with get_jwt_identity
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200


@app.route('/driver/database', methods=['GET', 'POST'])
def driver_databse_status():
    infoip = IP.split(".")
    if infoip[0] != infoip[-1]:
        partialip = infoip[0] + infoip[-1]
    else:
        partialip = infoip[0]
    resp = {'failed': (Neo4jx.session is None), 'info': partialip}
    return return_response(resp)


@app.route('/upload/csv/water-quality/data', methods=['POST'])
def file_recv_data():
    """ API endpoint that receives a csv with water analysis data
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated data
    """
    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']
        file = request.files[filename]

        print("receiving csv file at water-quality/data")

        if file and allowed_file(filename):
            df = read_topandas(request.files[filename], json=False)

            try:
                if (('id' not in df.columns) and ('snirh' not in df.columns)):
                    return return_error(406, "Missing water quality identifiers columns: 'id' or 'snirh'")
                if 'date' not in df.columns:
                    return return_error(406, "Missing water quality analysis date column")

                if ('id' in df.columns) and ('snirh' not in df.columns):
                    df_id = df[(df['id'].notna())]
                    # print("df_id shape", df_id.shape)
                elif ('id' not in df.columns) and ('snirh' in df.columns):
                    df_id = df[df['snirh'].notna()]
                    # print("df_id shape", df_id.shape)
                else:
                    df_id = df[(df['snirh'].notna()) | (df['id'].notna())]
                    # print("df_id shape", df_id.shape)

                df_date = df_id[pd.to_datetime(df_id['date'], format='%d/%m/%Y', errors='coerce').notna()]

                if df_date.empty:
                    return return_error(406, "Invalid date format")

                error_list = pandas_notsatisfied(df, df_date)
                dictObj = df_date.to_dict(orient='records')

            except KeyError:
                return return_error(406, "Invalid csv file where is missing either id or date columns")

            try:
                resp = Neo4jx.session.run(queries.QUERY_LOAD_DATA, json=dictObj).data()
            except AttributeError:
                return abort(400)

            return return_response(resp, error_list)

        flash('Allowed file types are shp, csv, json')

    return abort(403)


@app.route('/upload/csv/water-quality/stations', methods=['POST'])
def file_recv_station():
    """ API endpoint to receive an csv file that holds water stations info
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated station nodes
    """

    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']

        print('receiving csv files at water-quality/stations')

        try:
            df = read_topandas(request.files[filename], lower=True, json=False)
        except (TypeError, AttributeError):
            return return_error(406, "Invalid csv file")

        try:
            df_filtered = df[(df['id'].notna()) & (df['longitude'].notna()) & (df['latitude'].notna())]
            df_coord = df_filtered[pd.to_numeric(df_filtered['latitude'], errors='coerce').notnull() & pd.to_numeric(df_filtered['longitude'], errors='coerce').notnull()]

            if df_coord.empty:
                return return_error(406, "Invalid coordinates data type")

            error_list = pandas_notsatisfied(df, df_coord)
            dictObj = df_coord.to_dict(orient='records')

        except KeyError:
            return return_error(406, "Invalid csv file where is missing either id or coordinates columns")

        try:
            resp = Neo4jx.session.run(queries.QUERY_LOAD_STATION, json=dictObj).data()
        except AttributeError:
            return abort(400)

    return return_response(resp, error_list)


@app.route('/upload/csv/waternodes', methods=['POST'])
def file_recv_waternodes():
    """ API endpoint that receives a csv with waternodes - physical elements of a water diagram
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated nodes
    """

    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']

        print("validating csv file at csv/waternodes")

        validator = validCSV(validator={'header': ["id", "name", "type", "subtype", "watersystem",
                                                   "latitude", "longitude", "height", "power", "fwl", "tag"]})
        if not validator.verify_header(request.files[filename]):
            return return_error(406, "Invalid file header")

        dictObj, df = read_topandas(request.files[filename])

        if pandas_ruler_haveNull(df.loc[:, ['id', 'name', 'type', 'watersystem', 'latitude', 'longitude']]):
            return return_error(406, "Missing values at ['id', 'name', 'type', 'watersystem', 'latitude', 'longitude']")

        if not (pandas_ruler_isNumCol(df['longitude']) and pandas_ruler_isNumCol(df['latitude'])):
            return return_error(406, "Invalid coordinate values")

        try:
            resp = Neo4jx.session.run(queries.QUERY_LOAD_WATER, json=dictObj).data()
        except AttributeError:
            return abort(400)

    return return_response(resp)


@app.route('/upload/csv/hydronodes', methods=['POST'])
def file_recv_hydronodes():
    """ (NEED-NEW)API endpoint that receives a csv with waternodes - physical elements of a water diagram
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated nodes
    """

    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']

        print("validating csv file at csv/hydronodes")

        validator = validCSV(validator={'header': [
                             "id", "name", "type", "block", "watersystem", "tia", "min_alt", "max_alt"]})
        if not validator.verify_header(request.files[filename]):
            return return_error(406, "Invalid file header")

        dictObj, df = read_topandas(request.files[filename])

        if pandas_ruler_haveNull(df.loc[:, ['id', 'name', "block", 'type', 'watersystem']]):
            return return_error(406, "Missing values at ['id', 'name', 'block', 'type', 'watersystem']")

        try:
            resp = Neo4jx.session.run(queries.QUERY_LOAD_HYDRO, json=dictObj).data()
        except AttributeError:
            return abort(400)

    return return_response(resp)


@app.route('/upload/csv/allnodes', methods=['POST'])
def file_recv_allnodes():
    """ (NEED-NEW)API endpoint that receives a csv with waternodes - physical elements of a water diagram
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated nodes
    """

    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']

        dictObj, _ = read_topandas(request.files[filename])

        try:
            resp = Neo4jx.session.run(queries.QUERY_LOAD_ALLTYPE_NODES,
                                      json=dictObj).data()
        except AttributeError:
            return abort(400)

    return return_response(resp)


@app.route('/upload/csv/link/waternodes', methods=['POST'])
def file_recv_waternode_relations():
    """ API endpoint that receives a csv with realtionships between water nodes
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated relations
    """
    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']

        print("receiving csv file at link/waternodes")

        validator = validCSV(validator={'header': [
                             'start', 'end', 'type']})
        if not validator.verify_header(request.files[filename]):
            return return_error(406, "Missing header ['start', 'end', 'type']")

        dictObj, df = read_topandas(request.files[filename])
        if pandas_ruler_haveNull(df.loc[:, ['start', 'end', 'type']]):
            return return_error(406, "Missing values at ['start','end','type']")

        try:
            resp = Neo4jx.session.run(queries.QUERY_LOAD_RELATIONS, json=dictObj).data()
        except AttributeError:
            return abort(400)

    return return_response(resp)


@app.route('/upload/csv/link/all', methods=['POST'])
def file_recv_relations():
    """ API endpoint that receives a csv that describes the realtionships between
        waternodes, features and station. The relation can be: CONTAIN, RELATED or CONNECTED
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated relations
    """
    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']

        print("receiving csv file at link/all")

        validator = validCSV(validator={'header': [
                             'start', 'end', 'type']})

        if not validator.verify_header(request.files[filename]):
            return return_error(406, "Missing header ['start', 'end', 'type']")

        df = pd.read_csv(request.files[filename])
        dictObj = df.to_dict(orient='records')

        try:
            resp = Neo4jx.session.run(queries.QUERY_LOAD_RELATIONS_ALL,
                                      json=dictObj).data()
        except AttributeError:
            return abort(400)

    return return_response(resp)


@app.route('/upload/file', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']

        if '.' in filename:
            print("upload/file", filename)
            ext = filename.rsplit('.', 1)[1].lower()
            if ext in ALLOWED_EXTENSIONS:   # TO-DO convert this into a dict
                if ext == 'geojson':
                    print("receiving geoJSON file...")
                    return file_recv_geojson_elem()
                if ext == 'zip':
                    print("receiving zip file...")
                    return file_recv_shp_node()
                if ext == 'csv':
                    print("receiving csv file..")
                    validWaterNodes = validCSV(validator={'header': ["id", "name", "type", "subtype", "watersystem",
                                                                     "latitude", "longitude", "height", "power", "fwl", "tag"]})
                    validNodesLinks = validCSV(
                        validator={'header': ["start", "end", "type", "subtype", "flow_rate"]})

                    validHydroNodes = validCSV(validator={'header': [
                                               "id", "name", "type", "block", "watersystem", "tia", "min_alt", "max_alt"]})

                    validQualityStations = validCSV(
                        validator={'header': ["id", "latitude", "longitude"]})

                    validQualityData = validCSV(validator={'header': ["date"]})

                    if validWaterNodes.verify_header(request.files[filename]):
                        return file_recv_waternodes()

                    if validNodesLinks.verify_header(request.files[filename]):
                        return file_recv_waternode_relations()

                    if validHydroNodes.verify_header(request.files[filename]):
                        return file_recv_hydronodes()

                    if validQualityStations.simple_verify_header(request.files[filename]):
                        return file_recv_station()

                    if validQualityData.simple_verify_optional_header(request.files[filename], options=['id', 'snirh']):
                        return file_recv_data()

    return "Compatible resource not found", 404


@app.route('/upload/map/geojson', methods=['POST'])
def file_recv_geojson_elem():
    """ Receive geo-json file and create two types of nodes: the shapefile and features
        Each feature should have a unique id. If `id` is not provided by shapefile,
        the database will automatic assign an UUID.
            Args:
                no value
            Returns:
                Returns json with the name, id, category for each feature
                if query execution was sucessful
    """
    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))

        filename = data_header['data'][0]['name']
        fdescription = data_header['data'][0]['description']
        fcategory = data_header['data'][0]['category']
        file = request.files[filename]

        if file and allowed_file(filename):

            try:
                geodata = json.load(file)  # print("before:",geodata['features'][0]["properties"])
                for f in geodata['features']:
                    for key in f["properties"].keys():
                        new = {}
                        new[key.lower()] = f["properties"][key]  # f["properties"][key.lower()]= f["properties"].pop(key)
                        f["properties"] = new

            except (KeyError, ValueError):
                return return_error(406, "geoJSON not valid")

            # print("after:",geodata['features'][0]["properties"])
            try:
                resp = Neo4jx.session.run(queries.QUERY_LOAD_SHP_GEOJSON_V2, geojson=geodata, filename=filename,
                                          description=fdescription, category=fcategory).data()
            except AttributeError:
                return abort(400)

            # print(geodata)
            return return_response(resp)

        flash('Allowed file types are shp, csv, json')

    return abort(403)


@app.route('/upload/map/shp', methods=['POST', 'GET'])
def file_recv_shp_node():
    """ API endpoint that receives a zip with shp,sbf,shx files
        This endpoint receives a file via FormData only by a `POST` request
            Args:
                no value
            Returns:
                Returns json object with the number of created/updated data
    """
    if request.method == 'POST':

        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']
        fdescription = data_header['data'][0]['description']
        fcategory = data_header['data'][0]['category']
        file = request.files[filename]
        print("Receiving the file %s..." % filename)

        if file and allowed_file(filename):
            try:
                with (ZipMemoryFile(file)) as memfile:
                    with memfile.open() as src:
                        crs = src.crs
                        gdf = gpd.GeoDataFrame.from_features(src, crs=crs)
                        gdf.rename(columns=str.lower, inplace=True)

                geodata = gdf.to_json()
            except (TypeError, AttributeError):
                return return_error(406, "ERSI Shapefile zip not valid")

            # print(json.loads(geodata))
            try:
                resp = Neo4jx.session.run(queries.QUERY_LOAD_SHP_GEOJSON_V2, geojson=json.loads(geodata), filename=filename,
                                          description=fdescription, category=fcategory).data()
            except AttributeError:
                print(AttributeError, resp)
                return abort(400)

            return return_response(resp)

        flash('Allowed file types are shp, csv, json, zip')

    return abort(403)


@app.route('/upload/map', methods=['POST'])
def upload_file_map():
    if request.method == 'POST':
        content = request.form
        data_header = json.loads(content.get('JSON'))
        filename = data_header['data'][0]['name']
        # print('filename', filename)

        if '.' in filename:
            ext = filename.rsplit('.', 1)[1].lower()
            if ext in ALLOWED_EXTENSIONS:
                if ext == 'geojson':
                    return file_recv_geojson_elem()
                if ext == 'zip':
                    return file_recv_shp_node()

    return abort(404)


@app.route('/api/waternode/reservatory/', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_reservatories")
def get_reservatories():
    """ API endpoint to get data of water reservatories
            Args:
                no value
            Returns:
                Returns json object that may contain multiple dictionaries representing each reservatory
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_RESERVATORIES).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/waternode/relation', methods=['GET', 'POST'])
# @app.cache.cached(timeout=10, key_prefix="get_water_rel")
def get_water_rel():
    """ API endpoint to get all relationships with water nodes
           Args:
                no value
            Returns:
                Returns json object that may contain multiple dictionaries representing each relationship
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_ALL_WATER_REL).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/waternode/dam', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_dams")
def get_dams():
    """ API endpoint to get data of available dams
           Args:
                no value
            Returns:
                Returns json object that may contain multiple dictionaries representing each dam
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_DAMS).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/station/', methods=['GET', 'POST'])
@cross_origin()
def get_stations():
    """ API endpoint to get data of available water stations
            Args:
                no value
            Returns:
                Returns json object that may contain multiple dictionaries representing each station
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_STATIONS).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/station/watershed/all', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_stations_related_allwatershed")
def get_stations_related_allwatershed():
    """ API endpoint to get data of available water stations
            Args:
                no value
            Returns:
                Returns json object that may contain multiple dictionaries representing each station
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_STATIONS_RELATED_ALL_WATERSHED).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/station/watershed/id/<station_id>', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_stations_related_watershed")
def get_stations_related_watershed(station_id):
    """ API endpoint to get data of available water stations
            Args:
                no value
            Returns:
                Returns json object that may contain multiple dictionaries representing each station
    """
    try:
        resp = Neo4jx.session.run(
            queries.QUERY_GET_STATIONS_RELATED_WATERSHED_ID, id=station_id).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/station/watershed/name/<name>', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_stations_related_watershed_name")
def get_stations_related_watershed_name(name):
    """ API endpoint to get data of available water stations
            Args:
                no value
            Returns:
                Returns json object that may contain multiple dictionaries representing each station
    """
    try:
        resp = Neo4jx.session.run(
            queries.QUERY_GET_STATIONS_RELATED_WATERSHED_NAME, name=name).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/data/<nodeid>', methods=['GET', 'POST'])
def get_stations_data(nodeid):
    """ API endpoint to get  the forward path of a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths and the number of path collected
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_STATION_DATA, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/multiple/data/<path:subpath>', methods=['GET', 'POST'])
def get_mult_stations_data(subpath):
    """ API endpoint to get  the forward path of a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths and the number of path collected
    """
    content = subpath.split("/")  # ex: ["WQS_RPRISNIRH_21","WQS_RPRISNIRH_22"]

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_STATION_DATA_MULTIPLE_ID, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/attr/<nodeid>', methods=['GET', 'POST'])
def get_stations_data_att(nodeid):
    """ API endpoint to get  the forward path of a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths and the number of path collected
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_WATERDATA_ATT_ID, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/multiple/attr', methods=['GET', 'POST'])
def get_mult_stations_data_att():
    # Disable Using a conditional statement with a constant value in this function
    # pylint: disable=using-constant-test
    """ API endpoint to get  the forward path of a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths and the number of path collected
    """
    # content = subpath.split("/")  #  ex: ["WQS_RPRISNIRH_21","WQS_RPRISNIRH_22"]

    content = request.get_json()

    try:
        if not content and len(content) == 0:
            return return_error(404, "JSON content is empty")
    except TypeError as tyerr:
        print(tyerr)
        return return_error(406, "JSON content is not valid ex: ['WQS_RPRISNIRH_21','WQS_RPRISNIRH_22']")

    try:
        if len(executor.futures):
            if not executor.futures.done('neo'):
                return return_error(302, "Task is already being executed :" + executor.futures._state('neo'))

            future = executor.futures.pop('neo')
            res, code = future.result()
            if code == 102:
                return return_error(102, "The database is already executing some query")

            return return_response(res)

        # resp = Neo4jx.execute(queries.QUERY_GET_MULTIPLE_WATERDATA_ATT, json=content)
        future = executor.submit_stored('neo', Neo4jx.execute, queries.QUERY_GET_MULTIPLE_WATERDATA_ATT, content)

        if future.done:  # future._state = FINISHED

            resp, _ = future.result()
            executor.futures.pop('neo')

    except AttributeError as err:
        print(err)
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/collect/attr/data', methods=['POST', 'PUT'])
def get_station_data_by_att():

    try:
        content = request.get_json()  # [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19", attributes:["cianetos (mg/l)"]}]
        print("Content Received at /api/water-quality/collect/attr/data", content)
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_ATT_DATA_WATERDATA, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/collect/attr/data/<funct>', methods=['POST', 'PUT'])
def get_station_data_att_function(funct):

    set_query = {"min": queries.QUERY_GET_ATT_DATA_PERSTATION_MIN,
                 "max": queries.QUERY_GET_ATT_DATA_PERSTATION_MAX,
                 "avg": queries.QUERY_GET_ATT_DATA_PERSTATION_AVG}

    try:
        content = request.get_json()  # [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19", attributes:["cianetos (mg/l)"]}]
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    aggr_function = funct.split("/")[0]

    if aggr_function not in set_query:
        return return_error(404, "Aggregation function does not exists")

    try:
        resp = Neo4jx.session.run(set_query[aggr_function], json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/obj/attr/data', methods=['POST', 'PUT'])
def get_station_data_by_att_date():

    try:
        # [{id:"WQS_RPRISNIRH_18", date: "01/10/2006", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19",  date:"01/11/2018" , attributes:["cianetos (mg/l)"]}]
        content = request.get_json()
        print("Content Received at /api/water-quality/obj/attr/data", content)
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
                _ = obj['date']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_DATA_OBJ_PERSTATION, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/obj/attr/data/<funct>', methods=['POST', 'PUT'])
def get_station_data_att_obj_func(funct):

    # [{id : "WQS_RPRISNIRH_24",  attributes : ["profundidade (valor)"]},{id : "WQS_RPRISNIRH_14", attributes :["profundidade (valor)"]},
    # {id : "WQS_RPRISNIRH_24", attributes : ["profundidade (valor)"]},{id : "WQS_RPRISNIRH_19", attributes : ["profundidade (valor)"]}]

    set_query = {"min": queries.QUERY_GETT_DATA_OBJ_PERSTATION_MIN,
                 "max": queries.QUERY_GETT_DATA_OBJ_PERSTATION_MAX,
                 "avg": queries.QUERY_GETT_DATA_OBJ_PERSTATION_AVG}

    try:
        content = request.get_json()  # [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19", attributes:["cianetos (mg/l)"]}]
        print("At /api/water-quality/obj/attr/data/funct:", content)
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    aggr_function = funct.split("/")[0]

    if aggr_function not in set_query:
        return return_error(404, "Aggregation function does not exists")

    # print("Will execute", set_query[aggr_function])

    try:
        resp = Neo4jx.session.run(set_query[aggr_function], json=content).data()
        print("response", resp)
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/attr/data/<funct>', methods=['POST', 'PUT'])
def get_station_data_allatt_function(funct):

    set_query = {"min": queries.QUERY_GET_ATT_DATA_PERATT_MIN,
                 "max": queries.QUERY_GET_ATT_DATA_PERATT_MAX,
                 "avg": queries.QUERY_GET_ATT_DATA_PERATT_AVG}

    try:
        content = request.get_json()  # [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19", attributes:["cianetos (mg/l)"]}]
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    aggr_function = funct.split("/")[0]

    if aggr_function not in set_query:
        return return_error(404, "Aggregation function does not exists")

    try:
        resp = Neo4jx.session.run(set_query[aggr_function], json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/attr/data/dates', methods=['POST', 'PUT'])
def get_station_data_get_dates():

    try:
        content = request.get_json()  # [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19", attributes:["cianetos (mg/l)"]}]
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_ATT_DATA_DATES, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/timeseries/attr/data', methods=['POST', 'PUT'])
def get_station_data_by_att_xy():

    try:
        content = request.get_json()  # [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19", attributes:["cianetos (mg/l)"]}]
        print("Content Received at /api/water-quality/collect/attr/data", content)
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_TIMESERIES_XY_ATT_DATA, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/timeseries/attr/data/range', methods=['POST', 'PUT'])
def get_station_data_by_att_xy_range():

    try:
        # [{id:"WQS_RPRISNIRH_18", range:{min:"01/04/2018", max:"01/01/2019"}, attributes:["cianetos (mg/l)"]},
        # {id:"WQS_RPRISNIRH_19",range:{min:"01/04/2018", max:"01/01/2019"}, attributes:["cianetos (mg/l)"]}]
        content = request.get_json()
        print("Content Received at /api/water-quality/collect/attr/data/range", content)
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
                _ = obj['range']
                _ = obj['range']['min']
                _ = obj['range']['max']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", range: {min:, max:}, attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_TIMESERIES_XY_ATT_DATA_RANGE, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/water-quality/bubble/attr/data', methods=['POST', 'PUT'])
def get_station_data_by_att_bubble_children():

    try:
        content = request.get_json()  # [{id:"WQS_RPRISNIRH_18", attributes:["cianetos (mg/l)"]},{id:"WQS_RPRISNIRH_19", attributes:["cianetos (mg/l)"]}]
        print("Content Received at /api/water-quality/collect/attr/data", content)
        if not content:
            return return_error(404, "JSON content is empty")
        for obj in content:
            try:
                _ = obj['id']
                _ = obj['attributes']
            except KeyError:
                return return_error(406, "JSON content is not valid ex: [{id:"", attributes:[]}]")

    except (AttributeError, TypeError):
        return return_error(406, "Missing JSON content")

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_BUBBLE_CHILDREN_ATT_DATA, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/path/forward/<nodeid>', methods=['GET', 'POST'])
def get_path_forward(nodeid):
    """ API endpoint to get  the forward path of a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths and the number of path collected
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_PATH_FORWARD, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/waternode', methods=['GET', 'POST'])
def get_waternodes():
    """ Request data from Neo4jx driver. Get all waternodes
            Args:
                no value
            Returns:
                Returns json file if query execution was sucessful
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_WATERNODES).data()

    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/waternode/source/<nodeid>', methods=['GET', 'POST'])
def get_waternode_source(nodeid):
    """ API endpoint to get the source node of a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths and the source node
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_SOURCE_NODE, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/waternode/end/<nodeid>', methods=['GET', 'POST'])
def get_waternode_end(nodeid):
    """ API endpoint to get the source node of a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths and the source node
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_END_NODE, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/path/full/<nodeid>', methods=['GET', 'POST'])
def get_fullpath_node(nodeid):
    """ API endpoint to get the full path (begin to end) that contains a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of paths
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_FULL_PATH, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/path/nearby/<nodeid>', methods=['GET', 'POST'])
def get_nearby_nodes(nodeid):
    """ API endpoint to get the nearby relations and nodes
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain the incoming node
                (and respective relationship) and outgoing node (and relationship)
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_NEARBY_WATERNODE, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/relation/attributes/<label>', methods=['GET', 'POST'])
def get_relation_att(label):
    """ API endpoint to get the relationship attributes
            Args:
                label (string): type of relationship
            Returns:
                Returns json object that may contain the  attribute key and type
    """
    reltype = label.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_REL_ATT % reltype).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/node/attributes/<label>', methods=['GET', 'POST'])
def get_node_att(label):
    """ API endpoint to get the node attributes
            Args:
                label (string): type of node
            Returns:
                Returns json object that may contain the  attribute key and type
    """
    nodetype = label.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_NODE_ATT % nodetype).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/node/detach/', methods=['GET', 'POST'])
def get_node_detach():
    """ API endpoint to get all not connected nodes
            Args:
                label (string): type of node
            Returns:
                Returns json object that may contain the  attribute key and type
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_DETACH, ).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/find/all/<string_text>', methods=['GET', 'POST'])
def get_find_all(string_text):
    """ API endpoint to get all nodes and relationships that contain "string_text"
            Args:
                string_text (string): string target
            Returns:
                Returns json object that may contain a list of nodes/relations and the number of founds
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_FIND_ALL_STRING,
                                  string=string_text).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/find/nodes/<path:subpath>', methods=['GET', 'POST'])
def get_find_node(subpath):
    """ API endpoint to get all nodes with specific attributes
            Args:
                attr (string): should be a string  with similar pattern "nodetye/attr1-=-value/attr2->-value2" ,
                the first word will be the node type and next ones will be related the attributes.
                In each segment the "-" is used separate the condition from names and values.
                The "/" is the character that delimits the segments dedicated for specify the attributes and nodes
            Returns:
                Returns json object that may contain a list of nodes and the number of founds
    """

    content = subpath.split("/")
    constructor = ""
    nodetype = content[0]

    for s in content[1:]:
        sub = s.split("-")
        if (prepare_cypher(sub)) and (sub.size() != 3):
            return abort(404)
        constructor += "n." + sub[0] + " " + sub[1] + " " + sub[2] + " AND "

    try:
        resp = Neo4jx.session.run(queries.QUERY_FIND_NODE_ATT %
                                  (nodetype, constructor)).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/node/label/', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_nodes_label")
def get_nodes_label():
    """ API endpoint to get all labels/nodetypes
            Args:
                no value
            Returns:
                Returns json file available labels
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_NODE_LABEL).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/find/relation/<path:subpath>', methods=['GET', 'POST'])
def get_find_rel(subpath):
    """ API endpoint to get all relationships with specific attributes
            Args:
                attr (string): should be a string  with similar pattern "relltye/attr1-=-value/attr2->-value2" ,
                the first word will be the node type and next ones will be related the attributes.
                In each segment the "-" is used separate the condition from names and values.
                The "/" is the character that delimits the segments dedicated for specify the attributes and nodes
            Returns:
                Returns json object that may contain a list of relationships and the number of founds
    """

    content = subpath.split("/")
    constructor = ""
    reltype = content[0]

    for s in content[1:]:
        sub = s.split("-")
        if (prepare_cypher(sub)) and (sub.size() != 3):
            return abort(404)
        constructor += "n." + sub[0] + " " + sub[1] + " " + sub[2] + " AND "

    try:
        resp = Neo4jx.session.run(queries.QUERY_FIND_REL_ATT %
                                  (reltype, constructor)).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/watersystem/type', methods=['GET', 'POST'])
def get_watersystems():
    """ API endpoint to get all watersystems categories/types
            Args:
            Returns:
                Returns json object that may contain a list of dict
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_WATERSYSTEM).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/watersystem/nodes/<name>', methods=['GET', 'POST'])
def get_watersystems_node(name):
    """ API endpoint to get all watersystem's nodes
            Args:
            Returns:
                Returns json object that may contain a list of dict
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_WATERSYSTEM_NODES, name=name).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/watersystem/link/<name>', methods=['GET', 'POST'])
def get_watersystems_link(name):
    """ API endpoint to get all watersystem's relationships
            Args:
            Returns:
                Returns json object that may contain a list of dict
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_WATERSYSTEM_RELATIONS,
                                  name=name).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/map/type', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_geojson_layer")
def get_map_type():
    """ API endpoint to get all map types
            Args:
            Returns:
                Returns json object
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_SHP_TYPE).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/map/land-use/geojson/', methods=['GET', 'POST'])
@app.cache.cached(timeout=10, key_prefix="get_geojson_land_layer")
def get_geojson_land_layer():
    """ API endpoint to get all nodes type layer
            Args:
                nodeid (string): unique id
            Returns:
                Returns json object
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_ALL_LAYER_LAND).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/map/watershed/geojson/', methods=['GET', 'POST'])
def get_geojson_shed():
    """ API endpoint to get all nodes type watershed
            Args:
                nodeid (string): unique id
            Returns:
                Returns json object
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_ALL_WATERSHED).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/map/geometry/geojson/', methods=['GET', 'POST'])
def get_geojson_geometry_layer():
    """ API endpoint to get all nodes type element
            Args:
                nodeid (string): unique id
            Returns:
                Returns json object
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_ALL_GEOMETRY).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/map/geometry/<watersystem>/geojson/', methods=['GET', 'POST'])
def get_geojson_geometry_layer_watersystem(watersystem):
    """ API endpoint to get all nodes type layer
            Args:
                nodeid (string): unique id
            Returns:
                Returns json object
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_SHP_OF_WATERSYSTEM,
                                  name=watersystem).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/map/geometry/path/full/<nodeid>', methods=['GET', 'POST'])
def get_fullpath_shp(nodeid):
    """ API endpoint to get the geometry associated to full path (begin to end) that contains a specific node
            Args:
                nodeid (string): node unique id
            Returns:
                Returns json object that may contain a list of geojson
    """
    uid = nodeid.split("/")[0]
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_FULL_PATH_SHP_V2, id=uid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/find/map/type/<category>', methods=['GET', 'POST'])
def get_geojson_element_bycategory(category):
    """ API endpoint to get all geojosn by type
            Args:
                category (string): type
            Returns:
                Returns json object
    """
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_SHP_GEOJSON_BY_TYPE,
                                  category=category).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/find/map/target/<path:subpath>', methods=['GET', 'POST'])
def get_geojson_element_by_name_category(subpath):
    """ API endpoint to get geojson by  category and name
            Args:
                subpath (string): /category/name/
            Returns:
                Returns json object
    """
    content = subpath.split("/")
    try:
        category = content[0]
        name = content[1]
    except ValueError:
        abort(404)

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_GEOJSON_BY_TYPE_NAME,
                                  category=category, name=name).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/find/map/geojson/id/<nodeid>', methods=['GET', 'POST'])
def get_find_geojson_by_id(nodeid):
    """ API endpoint to get all nodes with specific id
            Args:
                nodeid (string): unique id
            Returns:
                Returns json object
    """

    # id_string = nodeid.split("/")
    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_FIND_SHAPEFILE_ID, id=nodeid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/api/find/map/geojson/target/<path:subpath>', methods=['GET', 'POST'])
def get_find_geojson_target_category_id(subpath):
    """ API endpoint to get features with specifc id and category
            Args:
                nodeid (string): unique id
            Returns:
                Returns json object
    """

    content = subpath.split("/")
    try:
        category = content[0]
        nodeid = content[1]
    except ValueError:
        abort(404)

    try:
        resp = Neo4jx.session.run(queries.QUERY_GET_FIND_SHAPEFILE,
                                  id=nodeid, category=category).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/connect/related/water-quality', methods=['GET', 'POST'])
def get_connect_station_shp():

    conn = []

    try:
        geoData = Neo4jx.session.run(queries.QUERY_GET_ALL_WATERSHED_ID).data()
    except AttributeError:
        abort(400)

    try:
        stationData = Neo4jx.session.run(queries.QUERY_GET_STATIONS).data()
    except AttributeError:
        abort(400)

    for shp in geoData:
        for p in stationData[0]['station']:
            try:
                if geojson.point_in_polygon(convert_toPoint(p['longitude'], p['latitude']), shp['geojson']['geometry']):
                    conn.append({'pid': p['id'], 'sid': shp['id']})
            except TypeError:
                pass

    try:
        resp = Neo4jx.session.run(queries.QUERY_SET_RELATED_SHP, list=conn).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/connect/related/waternodes', methods=['GET', 'POST'])
def get_connect_waternode_shp():

    conn = []

    try:
        geoData = Neo4jx.session.run(queries.QUERY_GET_ALL_WATERSHED_ID).data()
    except AttributeError:
        abort(400)

    try:
        waternodesData = Neo4jx.session.run(queries.QUERY_GET_WATERNODES).data()
    except AttributeError:
        abort(400)

    for shp in geoData:
        for p in waternodesData[0]['waternodes']:
            info = json.loads(json.dumps(p))
            try:
                result = geojson.point_in_polygon(convert_toPoint(
                    info['location'][0], info['location'][1]), shp['geojson']['geometry'])
                if result:
                    conn.append({'pid': info['id'], 'sid': shp['id']})
            except (TypeError, KeyError):
                pass

    try:
        resp = Neo4jx.session.run(queries.QUERY_SET_RELATED_SHP_WATERNODE,
                                  list=conn).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/connect/represented', methods=['GET', 'POST'])
def get_connect_node_shp_layer():

    conn = []

    try:
        geoData = Neo4jx.session.run(queries.QUERY_GET_ALL_GEOMETRY_ID).data()
    except AttributeError:
        abort(400)

    try:
        waternodesData = Neo4jx.session.run(queries.QUERY_GET_WATERNODES).data()
    except AttributeError:
        abort(400)

    for shp in geoData:
        for p in waternodesData[0]['waternodes']:
            info = json.loads(json.dumps(p))
            try:
                result = geojson.point_in_polygon(convert_toPoint(
                    info['location'][0], info['location'][1]), shp['geojson']['geometry'])

                if result:

                    conn.append({'pid': info['id'], 'sid': shp['id']})
            except (TypeError, KeyError):
                pass

    # print(conn)
    try:
        resp = Neo4jx.session.run(queries.QUERY_SET_REPRESENTED_SHP, list=conn).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/connect/<watersystem>/represented/', methods=['GET', 'POST'])
def get_connect_node_shp_by_watersys(watersystem):

    conn = []

    try:
        geoData = Neo4jx.session.run(queries.QUERY_GET_ALL_GEOMETRY_ID).data()
    except AttributeError:
        abort(400)

    try:
        waternodesData = Neo4jx.session.run(
            queries.QUERY_GET_WATERSYSTEM_NODES, name=watersystem).data()
    except AttributeError:
        abort(400)

    for shp in geoData:
        for p in waternodesData[0]['waternodes']:
            try:
                info = json.loads(json.dumps(p))
                if geojson.point_in_polygon(convert_toPoint(info['location'][0], info['location'][1]), shp['geojson']['geometry']):
                    conn.append({'pid': info['id'], 'sid': shp['id']})
            except (TypeError, KeyError):
                pass
    try:
        resp = Neo4jx.session.run(queries.QUERY_SET_REPRESENTED_SHP, list=conn).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/connect/contained', methods=['GET', 'POST'])
def get_connect_node_station():

    conn = []

    try:
        geoData = Neo4jx.session.run(queries.QUERY_GET_ALL_NODE_SHP).data()
    except AttributeError:
        abort(400)

    try:
        stationData = Neo4jx.session.run(queries.QUERY_GET_STATIONS).data()
    except AttributeError:
        abort(400)

    for shp in geoData:
        for p in stationData[0]['station']:
            try:
                if geojson.point_in_polygon(convert_toPoint(p['longitude'], p['latitude']), shp['geojson']['geometry']):
                    conn.append({'pid': p['id'], 'nid': shp['nid']})
            except (TypeError, KeyError):
                pass

    try:
        resp = Neo4jx.session.run(queries.QUERY_SET_CONTAINED_NODE_STATION,
                                  list=conn).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/connect/info', methods=['GET', 'POST'])
def get_connect_land_watershed():

    conn = []

    try:
        water = Neo4jx.session.run(queries.QUERY_GET_ALL_WATERSHED_ID).data()
    except AttributeError:
        abort(400)

    try:
        land = Neo4jx.session.run(queries.QUERY_GET_ALL_LAYER_LAND_ID).data()
    except AttributeError:
        abort(400)

    for watershed in water:
        for landuse in land:
            try:
                wtd = gpd.GeoDataFrame.from_features([watershed['geojson']])
                lde = gpd.GeoDataFrame.from_features([landuse['geojson']])
                if wtd.contains(lde)[0]:
                    conn.append({'wid': watershed['id'], 'lid': landuse['id']})
            except (TypeError, KeyError):
                pass

    # print(conn)
    try:
        resp = Neo4jx.session.run(
            queries.QUERY_SET_CONTAINED_LANDUSE_WATERSHED, list=conn).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/delete/relation/<id1>/<id2>', methods=['GET', 'POST'])
def get_delete_water_relation(id1, id2):

    try:
        resp = Neo4jx.session.run(queries.QUERY_DELETE_WATER_RELATION,
                                  id1=id1, id2=id2).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/delete/waternode/<nodeid>', methods=['GET', 'POST'])
def get_delete_waternode(nodeid):

    try:
        resp = Neo4jx.session.run(queries.QUERY_DELETE_WATERNODE, id=nodeid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/add/relation/waternode/<startid>/<endid>', methods=['GET', 'POST'])
def get_add_water_relation(startid, endid):

    try:
        resp = Neo4jx.session.run(queries.QUERY_ADD_NEW_WATERNODE_RELATION,
                                  startid=startid, endid=endid).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


@app.route('/resource/i/add/relation/waternode/<startid>/<endid>', methods=['POST', 'PUT'])
def get_add_waternode_relation(startid, endid):

    try:
        content = request.get_json()
        if not content:
            abort(404)
    except AttributeError:
        abort(400)

    try:
        resp = Neo4jx.session.run(queries.QUERY_ADD_NEW_MIDDLE_NODE_RELATION,
                                  startid=startid, endid=endid, json=content).data()
    except AttributeError:
        abort(400)

    return return_response(resp)


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True, port=5001, debug=True)
