# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2021-06-12

### Added

- Add simple implementation of Flask-jwt and User class for further development
- Add a Neo4j class wrapper to safely handle the Neo4j's python driver
- Add option to deploy to Heroku via CI/CD

### Changed

- Change relationship direction to reverse (From file to object). ref: QUERY_LOAD_SHP_GEOJSON_V2

### Removed

- Remove endpoint `/upload/csv/water-quality/data`

### Fixed

	-	Fix queries for get water quality station's data 



## [1.0.1] - 2021-05-24

### Added

- Add more Feedback to upload endpoint to know why the files are invalid

### Changed

- Change naming relationships for Map feature nodes: FEATURE to COMPOSED_BY and ELMENT to HAS
- Changed database status endpoint to return an object with `failed` instead `active`

### Removed

### Fixed

## [1.0.0] - 2021-04-20
### Added
- Upload API endpoint for CSV data: waternodes, hydronodes, node relationships, water quality stations and water quality data
- Upload API endpoint for GIS data: geojson and shapefile
- Flask cache
- CSV file header verification with [validCSV](https://github.com/bmalbusca/validCSV) package
- Conversion shapefile to geojson
- GIS utilities: geometry intersection

### Changed

### Removed

### Fixed