# API Endpoints

- `GET` http://0.0.0.0:5001/api/waternode  returns Array `waternodes : Array::dict`

  ```json
  [{"waternodes": [{"subtype": NaN, "name": "Alqueva", "location": [-7.496078, 38.206491, 143.0], "fwl": "152.0", "tag": "origin", "power": "520.0", "id": "WN_ALQUEVA_B0", "type": "Barragem"}, ...
  ```

- `GET` http://0.0.0.0:5001/api/waternode/relation returns `Array::dict`

  ```json
  [{"type": "Aduação", "subtype": "Bombagem", "flow": 42.0, "start": "WN_ALQUEVA_B0", "startcoord": {"lng": -7.496078, "lat": 38.206491}, "end": "WN_ALQUEVA_E4", "endcoord": {"lng": -7.575005, "lat": 38.337011}}, {"type": "Aduação", "subtype": "Bombagem", "flow": 37.0, "start": "WN_ALQUEVA_B1", "startcoord": {"lng": -7.614198, "lat": 38.333779}, "end": "WN_ALQUEVA_B2", "endcoord": {"lng": -7.738848, "lat": 38.365344}}, ...
  ```

- `GET` http://0.0.0.0:5001/api/waternode/dam/  returns Array: `waternodes : Array::dict`

- `GET` http://0.0.0.0:5001/api/water-quality/station/: returns  Array `station:Array::dict`

  ```json
  [{"station": [{"id": "WQS_RPRISNIRH_0", "snirh": "23M/15", "latitude": "38.395249999999997", "longitude": "-7.3880100000000004"}, {"id": "WQS_RPRISNIRH_1", "snirh": "25M/10", "latitude": "38.167290000000001", "longitude": "-7.4169700000000001"}, {"id": "WQS_SNIRHSUB_238", "snirh": "559/21", "latitude": "37.676650430279935", "longitude": "-10.399017160645057"}, {"id": "WQS_SNIRHSUB_239", "snirh": "512/213", "latitude": "38.07724227272996", "longitude": "-10.411309153246787"}, {"id": "WQS_SNIRHSUB_243", "snirh": "512/212", "latitude": "38.111407854961904", "longitude": "-10.412380367577214"}, {"id": "WQS_SNIRHSUB_244", "snirh": "480/70", "latitude": "38.32029444912412", "longitude": "-10.418838585088151"}, {"id": "WQS_SNIRHSUB_245", "snirh": "509/297", "latitude": "38.059735651273726", "longitude": "-10.410710623184439"},
  ```

- `GET` http://0.0.0.0:5001/api/waternode/source/WN_ALQUEVA_R9 : returns `path: Array::dict` and `source:dict`

  ```json
  [{"path": [{"subtype": NaN, "name": "Alqueva", "location": [-7.496078, 38.206491, 143.0], "fwl": "152.0", "tag": "origin", "power": "520.0", "id": "WN_ALQUEVA_B0", "type": "Barragem"}, "CONNECTED", {"subtype": "Primária", "name": "Alqueva-Alamos", "location": [-7.575005, 38.337011, 152.0], "fwl": "0.0", "tag": NaN, "power": "42.0", "id": "WN_ALQUEVA_E4", "type": "Estação Elevatória"}, "CONNECTED", {"subtype": NaN, "name": "Alamos", "location": [-7.614198, 38.333779, 208.0], "fwl": "227.5", "tag": "distribution", "power": "0.0", "id": "WN_ALQUEVA_B1", "type": "Barragem"}, "CONNECTED", {"subtype": NaN, "name": "Loureiro", "location": [-7.738848, 38.365344, 211.0], "fwl": "220.0", "tag": "distribution", "power": "0.0", "id": "WN_ALQUEVA_B2", "type": "Barragem"}, "CONNECTED", {"subtype": "Secundária", "name": "Loureiro-Alvito", "location": [-7.74176, 38.363315, 223.0], "fwl": "0.0", "tag": "distribution", "power": "0.8", "id": "WN_ALQUEVA_E5", "type": "Estação Elevatória"}, "CONNECTED", {"subtype": "ligação Abastecimento Público", "name": "Alvito", "fwl": "197.5", "location": [-7.887377, 38.299423, 187.0], "tag": "distribution", "power": "2.9", "id": "WN_ALQUEVA_B3", "type": "Barragem"}], "source": {"subtype": NaN, "name": "Alqueva", "location": [-7.496078, 38.206491, 143.0], "fwl": "152.0", "tag": "origin", "power": "520.0", "id": "WN_ALQUEVA_B0", "type": "Barragem"}}]
  ```

- `GET` http://0.0.0.0:5001/api/map/watershed/geojson/ : return `` Array::dict ``

  ```json
  [{"geojson": {"geometry": {"coordinates": [[[-8.057161750908856, 38.24798898668748], [-8.057245704905107, 38.24778030449486], [-8.057354411326804, 38.24771713214862], [-8.057431774147515, 38.2476945696889],  38.248430765463766], [-8.05790440592625, 38.24855486420125], [-8.057875687563499, 38.24859772821138], [-8.057841399287806, 38.24861356190954], [-8.057807022504095, 38.24860444085773], [-8.057266588417496, 38.248351302815514], [-8.057100692633952, 38.24826092620108], [-8.057077910670763, 38.248197668886135], [-8.057161750908856, 38.24798898668748]]], "type": "Polygon"}, "id": "5654", "type": "Feature", "properties": {"AREA": 16398.68265003586, "COS2018_n1": "9.Massas de água superficiais", "COS2018_Lg": "9.1.2.5 Charcas", "COS2018_n4": "9.1.2.5", "Name": "Odivelas"}}}, {"geojson": {"geometry": {"coordinates": [[[-7.962646045380352, 38.0154769467153], [-7.962667068597914, 38.01544923767282], [-7.962699951383006, 38.01550150571206], [-7.9627178983974005, 38.0155300343191], [-7.962841575801062, 38.0155762507888], [-7.963004873602134, 38.015620946025756], [-7.963074534901666, 38.01564001307158], [-7.963326558506921,
  ```

- `GET` http://0.0.0.0:5001/api/find/map/geojson/target/watershed/82bab7bf-e15f-43d9-a968-7640eee7fa11

  ```json
  [{"geojson": {"geometry": {"coordinates": [[[-8.057161750908856, 38.24798898668748], [-8.057245704905107, 38.24778030449486], [-8.057354411326804, 38.24771713214862], [-8.057431774147515, 38.2476945696889], [-8.057500433718777, 38.24769236171332], [-8.05766779054419, 38.24769472163796], [-8.057736454760269, 38.24768800907114], [-8.05779086250887, 38.24765867494589], [-8.057879704763337, 38.247581975993285], [-8.057945580253984, 38.24750985681132], [-8.058146048193445, 38.24730683404395], [-8.05819180819773,  38.248351302815514], [-8.057100692633952, 38.24826092620108], [-8.057077910670763, 38.248197668886135], [-8.057161750908856, 38.24798898668748]]], "type": "Polygon"}, "id": "5654", "type": "Feature", "properties": {"AREA": 16398.68265003586, "COS2018_n1": "9.Massas de água superficiais", "COS2018_Lg": "9.1.2.5 Charcas", "COS2018_n4": "9.1.2.5", "Name": "Odivelas"}}}]
  ```

- `GET` http://localhost:5001/api/path/nearby/WN_ALQUEVA_B3

  ```json
  [{
  	"nearby": [{
  		"from": {
  			"subtype": "Primária",
  			"name": "Alqueva-Alamos",
  			"fwl": "0.0",
  			"location": [-7.575005, 38.337011, 152.0],
  			"power": "42.0",
  			"id": "WN_ALQUEVA_E4",
  			"tag": 0,
  			"type": "Estação Elevatória"
  		},
  		"to": {
  			"subtype": 0,
  			"name": "Alamos",
  			"fwl": "227.5",
  			"location": [-7.614198, 38.333779, 208.0],
  			"tag": "distribution",
  			"id": "WN_ALQUEVA_B1",
  			"power": "0.0",
  			"type": "Barragem"
  		},
  		"info": [{
  			"subtype": "Primária",
  			"name": "Alqueva-Alamos",
  			"fwl": "0.0",
  			"location": [-7.575005, 38.337011, 152.0],
  			"power": "42.0",
  			"id": "WN_ALQUEVA_E4",
  			"tag": 0,
  			"type": "Estação Elevatória"
  		}, "CONNECTED", {
  			"subtype": 0,
  			"name": "Alamos",
  			"fwl": "227.5",
  			"location": [-7.614198, 38.333779, 208.0],
  			"tag": "distribution",
  			"id": "WN_ALQUEVA_B1",
  			"power": "0.0",
  			"type": "Barragem"
  		}]
  	},...
  ```

- `GET` http://localhost:5001/api/path/forward/WN_ALQUEVA_B3

  ```json
  [{
  	"path": [
  		[{
  			"subtype": "ligação Abastecimento Público",
  			"name": "Alvito",
  			"fwl": "197.5",
  			"location": [-7.887377, 38.299423, 187.0],
  			"power": "2.9",
  			"id": "WN_ALQUEVA_B3",
  			"tag": "distribution",
  			"type": "Barragem"
  		}, "CONNECTED", {
  			"subtype": 0,
  			"name": "Monte Novo R1",
  			"fwl": "0.0",
  			"location": [-7.784449, 38.437208, 214.0],
  			"tag": "distribution",
  			"id": "WN_ALQUEVA_R7",
  			"power": "0.0",
  			"type": "Reservatório"
  		}], ...
  ```

- `GET` http://localhost:5001/api/waternode/source/WN_ALQUEVA_B3

  ```json
  [{
  	"path": [{
  		"subtype": 0,
  		"name": "Alqueva",
  		"fwl": "152.0",
  		"location": [-7.496078, 38.206491, 143.0],
  		"tag": "origin",
  		"id": "WN_ALQUEVA_B0",
  		"power": "520.0",
  		"type": "Barragem"
  	}, "CONNECTED", {
  		"subtype": "Primária",
  		"name": "Alqueva-Alamos",
  		"fwl": "0.0",
  		"location": [-7.575005, 38.337011, 152.0],
  		"power": "42.0",
  		"id": "WN_ALQUEVA_E4",
  		"tag": 0,
  		"type": "Estação Elevatória"
  	}, "CONNECTED", {
  		"subtype": 0,
  		"name": "Alamos",
  		"fwl": "227.5",
  		"location": [-7.614198, 38.333779, 208.0],
  		"tag": "distribution",
  		"id": "WN_ALQUEVA_B1",
  		"power": "0.0",
  		"type": "Barragem"
  	}, "CONNECTED", {
  		"subtype": 0,
  		"name": "Loureiro",
  		"fwl": "220.0",
  		"location": [-7.738848, 38.365344, 211.0],
  		"tag": "distribution",
  		"id": "WN_ALQUEVA_B2",
  		"power": "0.0",
  		"type": "Barragem"
  	}, "CONNECTED", {
  		"subtype": "Secundária",
  		"name": "Loureiro-Alvito",
  		"fwl": "0.0",
  		"location": [-7.74176, 38.363315, 223.0],
  		"tag": "distribution",
  		"id": "WN_ALQUEVA_E5",
  		"power": "0.8",
  		"type": "Estação Elevatória"
  	}, "CONNECTED", {
  		"subtype": "ligação Abastecimento Público",
  		"name": "Alvito",
  		"fwl": "197.5",
  		"location": [-7.887377, 38.299423, 187.0],
  		"power": "2.9",
  		"id": "WN_ALQUEVA_B3",
  		"tag": "distribution",
  		"type": "Barragem"
  	}],
  	"source": {
  		"subtype": 0,
  		"name": "Alqueva",
  		"fwl": "152.0",
  		"location": [-7.496078, 38.206491, 143.0],
  		"tag": "origin",
  		"id": "WN_ALQUEVA_B0",
  		"power": "520.0",
  		"type": "Barragem"
  	}
  }]
  ```

- `GET` http://localhost:5001/api/water-quality/data/WQS_RPRISNIRH_3

  ```json
  [{
  	"data": {
  		"date": "38419.0",
  		"OD (In Situ) (mg/L)": "mg/L",
  		"OD mg/L (In Situ) (valor)": "9.0",
  		"Potencial Redox (In Situ) (observações)": 0,
  		"Campanha": "38412",
  		"pH (In Situ) (Escala Sorensen)": "Escala Sorensen",
  		"Turbidez (In Situ) (observações)": 0,
  		"Objectivo": "Cargas Afluentes",
  		"Temperatura (In Situ) (ºC)": "ºC",
  		"Potencial Redox (In Situ) (valor)": "36.0",
  		"Profundidade (S/M/F)": 0,
  		"EDIA": "AQV-101-333-070-032",
  		"Turbidez (In Situ) (valor)": 0,
  		"pH (In Situ) (valor)": "7.9299999999999997",
  		"Caracteristicas na amostragem": "Albufeira",
  		"OD (In Situ) (% saturação)": "%",
  		"Condutividade (In Situ) (microS/cm)": "microS/cm",
  		"Turbidez (In Situ) (NTU)": 0,
  		"OD % (In Situ) (valor)": "80.29999999999998",
  		"Época": "Húmida",
  ```
  
- `POST` http://0.0.0.0:5001/api/water-quality/collect/attr/data/avg

  - Body: content-type/json

    ```json
    [
        {
            "id": "WQS_RPRISNIRH_18",
            "attributes": [
                "azoto amoniacal (nh4) (valor)",
                "condutividade (in situ) (valor)"
            ]
        },
        {
            "id": "WQS_RPRISNIRH_19",
            "attributes": [
                "condutividade (in situ) (valor)"
            ]
        }
    ]
    ```

    

  - Response

    ````json
    [
        {
            "id": "WQS_RPRISNIRH_18",
            "attributes": [
                {
                    "name": "azoto amoniacal (nh4) (valor)",
                    "value": 0.94
                },
                {
                    "name": "condutividade (in situ) (valor)",
                    "value": 643.0
                }
            ]
        },
        {
            "id": "WQS_RPRISNIRH_19",
            "attributes": [
                {
                    "name": "condutividade (in situ) (valor)",
                    "value": 578.9407
                }
            ]
        }
    ]
    ````

- `POST` http://0.0.0.0:5001/api/water-quality/attr/data/avg

  - Body: content-type/json

    ```json
    [
        {
            "id": "WQS_RPRISNIRH_18",
            "attributes": [
                "cianetos (mg/l)"
            ]
        },
        {
            "id": "WQS_RPRISNIRH_19",
            "attributes": [
                "cianetos (mg/l)"
            ]
        }
    ]
    ```

  - Response

    ```json
    [
        {
            "name": "cianetos (mg/l)",
            "value": null
        }
    ]
    ```

- `POST` http://0.0.0.0:5001/api/water-quality/obj/attr/data/avg

  - Body: content-type/json

    ```json
    [
       {
          "id":"WQS_RPRISNIRH_24",
          "attributes":[
             "profundidade (valor)"
          ]
       },
       {
          "id":"WQS_RPRISNIRH_14",
          "attributes":[
             "profundidade (valor)"
          ]
       },
       {
          "id":"WQS_RPRISNIRH_19",
          "attributes":[
             "profundidade (valor)"
          ]
       }
    ]
    ```

  - Response

    ```json
    [
        {
            "keys": [
                "profundidade (valor)"
            ],
            "data": [
                {
                    "profundidade (valor)": null,
                    "id": "WQS_RPRISNIRH_24"
                },
                {
                    "profundidade (valor)": 8.369268783184816,
                    "id": "WQS_RPRISNIRH_14"
                },
                {
                    "profundidade (valor)": null,
                    "id": "WQS_RPRISNIRH_19"
                }
            ]
        }
    ]
    ```

- `POST` http://0.0.0.0:5001/api/water-quality/timeseries/attr/data

  - Body: same as before

  - Response

    ```json
    [
        {
            "id": "azoto amoniacal (nh4) (valor)",
            "key": "WQS_RPRISNIRH_18",
            "data": [
                {
                    "x": "01/06/2019",
                    "y": " "
                },
                {
                    "x": "01/03/2019",
                    "y": "0.029999999999999999"
                },
                {
                    "x": "01/03/2015",
                    "y": "0.0167"
                },
      ...
    ```

- `POST` http://0.0.0.0:5001/api/water-quality/bubble/attr/data

  - Body: same as before

  - Response

    ```json
    [
        {
            "id": "WQS_RPRISNIRH_18",
            "children": [
                {
                    "id": "azoto amoniacal (nh4) (valor)",
                    "value": 68,
                    "children": [
                        {
                            "value": 28,
                            "id": " "
                        },
                        ...
    ```

- `POST` 

  - Body: same as before

  - Response

    ```json
    [
        {
            "dates": [
                "01/10/2006",
                "15/11/2006",
                "01/01/2007",
                "01/04/2007",
                "01/10/2007",
                "01/11/2007",
                "01/12/2007",
                "01/01/2008",
                "14/01/2008",
                "01/02/2008",
                "01/03/2008",
                "01/09/2008",
                "01/10/2008",
                "01/11/2008",
                "01/12/2008",
                "01/01/2009",
                "01/02/2009",
                "01/03/2009",
                "01/04/2009",
                "01/05/2009",
            ]
        }
    ]
    ```

    

# Upload templates



### Nodes links

​	`` start,end,type,subtype,flow_rate``

​	requirements: require exact header, but can be unordered

### Water Nodes

​	``id,name,type,subtype,watersystem,latitude,longitude,height,power,fwl,tag``

​	requirements: require exact header, but can be unordered; cannot have null values on latitude,longitude,height

### Hydrographic Nodes

​	``id,name,type,block,watersystem,tia,min_alt,max_alt``

​	requirements: require exact header, but can be unordered

### Water Quality Stations

​	``id,snirh,latitude,longitude, ...``

​	requirements: require exact those four columns; all header names need to be lower case

### Water Quality Data

​	``id, date, snirh(optional), ...``

​	requirements: `date` format  `dd/mm/yy` ; all header names need to be lower case; data must be converted to float; Must have one of {id, snirh}

### GIS Data

All files should have lower case columns

##### Land-use

        | Field Name | Description | Format   |
        | ---------- | ----------- | -------- |
        | COSxxxx_n1 |             | String   |
        | COSxxxx_n4 |             | String   |
        | COSxxxx_Lg |             | String   |
        | AREA       |             | Float    |
        | Name       |             | String   |
        | geometry   |             | `object` |
##### Geometry

        | Field Name | Description                                    | Format   |
        | ---------- | ---------------------------------------------- | -------- |
        | Name       |                                                | String   |
        | infraest   | {Albufeira,Reservatório,Açude}                 | String   |
        | popupinfo  | Aditional info related to the geometry element | String   |
        | geometry   |                                                | `object` |



