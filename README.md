## Installation

1. ​	(optional) Install virtualenv package: `pip3 install virtualenv`

   1.1	Go to `src` folder: `cd ./thesis-project/backend/api-server`

   1.2	Create environment `.env`: `virtualenv .env`

   1.3	Activate environment: `source .env/bin/activate`



2. Install requirements:

   2.1	`pip3 install --upgrade -r api-server/requirements.txt`
   
   If you decide to install manually each package, try [`pipreqs`](https://github.com/bndr/pipreqs) to install the `api-server`, I would suggest  to you use [`conda`](https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/installing-with-conda.html). 
   
   

3. Test installation

   3.1	Start server:  ``python3 api-server/server.py``



## Development

### Conventions

- Style
  - [Google Python Style Guide](http://google.github.io/styleguide/pyguide.html)
    - [Napoleon (support)]( https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)



### Testing

For testing the API endpoints we choose `pytest`. The test are located at `'tests/'` folder and the configuration file `conftest.py`  is also, located at the same folder  (in the tests folder).

The linting tools used in CI are `flake8` and `pylint`.

   - Linting commands:

     -  [pylint:](https://www.pylint.org/) `~$ pylint --disable=C0103,C0114,C0116,W0613,R0201,C0302,R0911 --max-line-length=140 ./api-server/server.py`

     - [flake8:](https://gitlab.com/pycqa/flake8) `~$ flake8 --ignore W291,E305,E501,E251 ./api-server/server.py`

     Use [pylint-errors](https://vald-phoenix.github.io/pylint-errors/) and [autopep8](https://pypi.org/project/autopep8/) to debug warnings and errors at linting stage. Using autopep8:  ` autopep8 ./*/*.py -r -d --max-line-length 140`

     > *Tip:* Use your python's path env reference to run your commands `~$ python3 -m `

     

- Test configuration:

  The endpoints testing should be added to `test_routes.py` and all test funtions need to start by the prefix `test_`. When developing the test functions see the  [official pytest documention](https://docs.pytest.org).

  

  - Testing commands: 
    - `~$ python3 -m pytest`
  - Examples:
    - [pytest articles](https://smirnov-am.github.io/pytest-why-testing/)
    - [flask-pytest-example](https://github.com/aaronjolson/flask-pytest-example)



### Git branches and CI pipeline

#### Git branch convention

###### Principal 

-   `master` : current stable version ready to be deployed. All stable and mature code at `develop` branch will be merged to `master`.
-   `develop` : beta version and current development version.

###### Support

-   `feature-*` : new features development branch. the naming should be coherent with the scope , ex: ``feature-<framework>-<functionality>`. the features branches are used to integrate a new feature to the development branch.
-   hot-fix branches are created via issue's auto merge requests. 



#### CI pipeline

-   `develop` 

    ​	Pipeline:`linting >> testing >> deploy `

-   `master`

    ​	Pipeline: `linting >> testing >> release >> deploy`

    If the deployment is successful at `develop` branch and the current version on master (after the merging from `develop`)  checks all requirements,  a new release tag will be created. The tag creation is manual and the tag convention for a deployed version from `master` ( referred deploy to an infrastructure) is `v.{major}.{minor}.{revision/patch}` . The `<major>` is the version of current development and `<minor>` is the version's iteration.

    

    ##### Git release tag

    -   show existing tags: `git tag` 
    -   create tag: `git tag -a <tag> -m <text> ; Where` `<tag> ` can be ` v1.4` (`v.{major}.{minor}.{revision}`) and  `<text>` as annotation "my version 1.4.0"`
    -   see the new tag: `git show <tag>`
    -   git push the new tag: `git push -u <tag>`  or `git push -u --tags`


### Easy picks for your open-source contribution

- Change endpoints that sends earger queries to Neo4j to an async version. 

  The main goal is having a non-blocking endpoint for large work queries, in other words, usage of thread to handle

  this long (time) queries. For solving this problem, I found  [Flask-Executor](https://pypi.org/project/Flask-Executor/) and I also tested a simple implementation on this project at `/api/water-quality/multiple/attr` (see branch [feature-flaskexecutor-thread](https://gitlab.com/thesis-4j/backend-ediawater/-/tree/feature-flaskexecutor-thread) , at api-server/app.py ,starting around 930 line) :

  

  ```python
  Future Feature:
  
      * Use flask-executor to handle long queries. Usage example:
  
          from flask_executor import Executor
          app.config['EXECUTOR_PROPAGATE_EXCEPTIONS'] = True
          executor = Executor(app)
  
          ...
  
           try:
              if len(executor.futures):
                  if not executor.futures.done('neo'):
                      return return_error(302, "Task is already being executed :" + executor.futures._state('neo'))
  
                  future = executor.futures.pop('neo')
                  res, code = future.result()
                  if code == 102:
                      return return_error(102, "The database is already executing some query")
  
                  return return_response(res)
  
              # resp = Neo4j.execute(queries.QUERY_GET_MULTIPLE_WATERDATA_ATT, json=content)
              future = executor.submit_stored('neo', Neo4j.execute, queries.QUERY_GET_MULTIPLE_WATERDATA_ATT, content)
  
              if future.done:  # future._state = FINISHED
  
                  resp, _ = future.result()
                  executor.futures.pop('neo')
  
          except AttributeError as err:
              print(err)
              abort(400)
  
          return return_response(resp)
  
  ```

  The other alternative to this is using [asyncio](https://docs.python.org/3/library/asyncio.html) on neo4j python driver executor. Follow this [Neo4j's Github issue](https://github.com/neo4j/neo4j-python-driver/issues/180)

  

## Deployment

### Azure

1.  Clone the git repository to your azure directory (via console)

2.  Enter to repository folder and install the app requirements: `pip install --upgrade -r requirements.txt `

3.  Try to run your app first using the development flask web server

    -   Using only `flask run` (used by azure web server):

        1.  Remove `if __name__ == '__main__':` 

            >   If your are using a different name than `app.py` for your app file, you need to specify the file name: `export FLASK_APP="module:name"`
            >
            >   Test the configuration with gunicorn. Make sure you have gunicorn installed, via requirements or via source: `pip3 install git+https://github.com/benoitc/gunicorn.git`
            >
            >   ```python
            >   # If application.py
            >   gunicorn --bind=0.0.0.0 --timeout 600 application:app
            >   
            >   # If app.py
            >   gunicorn --bind=0.0.0.0 --timeout 600 app:app
            >   ```

            

    -   (Recommended) [Using both](https://www.twilio.com/blog/how-run-flask-application) `flask.run()` and `app.run()`  (`python3 app.py`)
        
        1.  Keep  `if __name__ == '__main__':`  and  `export FLASK_APP=app.py`
        2.  Then, use `flask.run()` or `python3 app.py` to run your application

    

4.   Add web app (container) resource via Marketplace

     Here your will create a `Resource group` , `App name` and set the server and container OS configuration

5.  [Deploy your app](https://docs.microsoft.com/en-us/azure/app-service/quickstart-python?tabs=bash&pivots=python-framework-flask): `az webapp up --sku B1 --name <app-name>` (you should be located at same folder as `app.py` when running this command)

##### Azure cli (After setting `Resource group` and `Web app` on Azure cloud shell)

It's possible to create a resource group and a web app service via Azure cli, but if this is your first time working with Azure I recommend you to follow the steps for deployment with Azure cloud portal first.

1.  (only once) Create [service principal](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli#code-try-0): `az ad sp create-for-rbac --name ServicePrincipalName` .Save the output, there you will find `"password"` , `"name"` (`<app-name-url-principal>`) and `"tenant"` (`<tenantid>`)

2.   [Login](https://docs.microsoft.com/en-us/cli/azure/authenticate-azure-cli) at your azure cli:  `az login --service-principal -u <app-name-url> -p <password-or-cert> --tenant <tenantid>`

3.  Deploy

    -   [Deploy your code from git](https://docs.microsoft.com/en-us/cli/azure/webapp/deployment/source?view=azure-cli-latest) : `az webapp deployment source config --branch master --manual-integration --name [myWebApp] --repo-url [git url] --app-working-dir [folder] --resource-group [myResourceGroup]` ; if your app folder is not root, you need to set at[`.deployment` file](https://github.com/projectkudu/kudu/wiki/Customizing-deployments). You need to specify your deploy command.

    -   Deploy App via your local repo:  `az webapp up  --name <app-name> -g <resource-group>`

4.  (optional) Connect to your app container: `az webapp ssh --name [our-app-service-name] -g [our-resource-group-name]` . You can specify the connection time adding `-t <sec>`

If you are using Git CI to deploy the app, make sure that environment variables are assigned by the value and not with a string (without quotes). Also, it's possible to set environment variable to azure using [`az webapp config appsettings`](https://docs.microsoft.com/en-us/cli/azure/webapp/config/appsettings?view=azure-cli-latest)

###### [Enable firewall port](https://docs.microsoft.com/en-us/cli/azure/network/nsg/rule?view=azure-cli-latest)

1.  Create Security group  `az network nsg create -g <resource-group> -n <MyNsg>`

2.  Create Network Security rule `az network nsg rule create -g <resource-group> --nsg-name <MyNsg> -n <MyNsgRule> --priority 100`

    -   ```bash
        az network nsg rule create -g api-flask-resource-group --nsg-name outgoing -n AllowBolt --priority 500 --source-address-prefixes '*' --destination-port-ranges 80 8080 7687 --access Allow --protocol Tcp --description "Allow Internet on ports 80,8080,7687."
        ```

        

###### [Enable CORS](https://docs.microsoft.com/en-us/azure/azure-functions/functions-how-to-use-azure-function-app-settings?tabs=portal#cors)

See this [github page guide](https://github.com/uglide/azure-content/blob/master/articles/app-service-api/app-service-api-cors-consume-javascript.md) and this [official documentation](https://docs.microsoft.com/en-us/azure/app-service/app-service-web-tutorial-rest-api#add-cors-functionality)

-   `az functionapp cors add -n <app-name> --resource-group <resource-group> --allowed-origins https://ediawater-app-dev.herokuapp.com/`

>   ​	Make sure that you inserted HTTPS url's and not HTTP.

### Heroku (CI/CD)

The deployment configuration  to Heroku will be focused on Gitlab CI/CD auto deployment. In this case we will use gitlab's tool `dpl`. But, the following procedures are also compatible if  you want to deploy manually.

1. (Install app and) Make sure that you can serve the app with gunicorn locally:  `gunicorn --bind 0.0.0.0:5000 app:app`

2. Prepare you `.yml` file to deploy the app

   Take a look at [DPL Heroku documentation](https://www.rubydoc.info/gems/dpl/1.10.16#heroku). Note: "Use the `--skip_cleanup=true` flag to deploy from the current file state. Note that many providers deploy by git and could ignore this option".

   - After that been said, Use gitlab-CI to create your `.env` file and `Procfile`.  Generate the `Procfile` by executing the following command: `echo "web: gunicorn app:app" > Procfile`

   

   

   

   

   

   

   